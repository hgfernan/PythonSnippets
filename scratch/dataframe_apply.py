#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 18:54:46 2024

@author: hilton
"""

import pandas as pd

df = pd.DataFrame({
    'A': [1, 2, 3],
    'B': [4, 5, 6]
})

# Apply a lambda function to each element
df['A_plus_B'] = df.apply(lambda row: row['A'], axis=1)
print(df)