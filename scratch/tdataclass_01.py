#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test of dataclass module
Created on Sat Nov  9 14:28:20 2024

@author: hilton
"""

import sys       # argv, exit()
import dataclasses # @dataclass, field()

@dataclasses.dataclass
class WithDataclass:
    """
    Subclass to hold excess parameters
    """
    f6 : float = dataclasses.field(default=4.0)
    f7 : float = dataclasses.field(default=3.0)
    f8 : float = dataclasses.field(default=2.0)
    f9 : float = dataclasses.field(default=1.0)

    def __call__(self):
        return WithDataclass()

@dataclasses.dataclass
class WithDataclass02:
    """
    Main class that holds parameters and delegates excess parameters
    to subclass
    """
    f0 : float = dataclasses.field(default=10.0)
    f1 : float = dataclasses.field(default=9.0)
    f2 : float = dataclasses.field(default=8.0)
    f3 : float = dataclasses.field(default=7.0)
    f4 : float = dataclasses.field(default=6.0)
    f5 : float = dataclasses.field(default=5.0)

    wdc : WithDataclass = \
        dataclasses.field(default_factory=WithDataclass())

class WithoutDataclass:
    """
    A class with excess parameters to generate the pylint warning about
    excess parameters
    """
    def __init__(self):
        self.f0 = 10.0
        self.f1 = 9.0
        self.f2 = 8.0
        self.f3 = 7.0
        self.f4 = 6.0
        self.f5 = 5.0
        self.f6 = 4.0
        self.f7 = 3.0
        self.f8 = 2.0
        self.f9 = 1.0

    def __repr__(self) -> str:
        result : str = ''

        result += type(self).__name__ + '('
        result += str(self)
        result += ')'

        # Normal function termination
        return result


    def __str__(self) -> str:
        result : str = ''

        result += f'{self.f0}, {self.f1}, {self.f2}, {self.f3}, {self.f4}, '
        result += f'{self.f5}, {self.f6}, {self.f7}, {self.f8}, {self.f9}'

        # Normal function termination
        return result


def main() -> int:
    w = WithDataclass02()
    print(f'w\n{w}')

    wo = WithoutDataclass()
    print(f'wo\n{wo}')
    
    print(f'wo == eval(repr(wo)\n{ wo == eval(repr(wo)) }')

    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main())
