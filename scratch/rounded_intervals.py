#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Code provided by ChatGPT to generate rounded intervals

Created on Mon Mar 18 18:18:38 2024

@author: GPT-3
"""

import math

def generate_rounded_intervals(data_range, num_intervals):
    min_value, max_value = data_range
    magnitude = math.floor(math.log10(max_value - min_value))  # Determine the magnitude of the range
    interval_width = (max_value - min_value) / num_intervals
    
    rounded_intervals = []
    for i in range(num_intervals):
        interval_start = round(min_value + i * interval_width, magnitude)
        interval_end = round(min_value + (i + 1) * interval_width, magnitude)
        rounded_intervals.append((interval_start, interval_end))
    
    return rounded_intervals

# Example usage
data_range = (950, 1000)  # Example data range with different orders of magnitude
num_intervals = 10
rounded_intervals = generate_rounded_intervals(data_range, num_intervals)
print("Rounded Intervals:")
for interval in rounded_intervals:
    print(interval)
