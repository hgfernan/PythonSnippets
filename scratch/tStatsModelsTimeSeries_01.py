#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 14:01:20 2024

@author: Jason Brownlee PhD, Sachin Date 
From:
* https://machinelearningmastery.com/decompose-time-series-data-trend-seasonality/
* https://timeseriesreasoning.com/contents/holt-winters-exponential-smoothing/
"""

import pandas as pd
from matplotlib import pyplot
from statsmodels.tsa.seasonal import seasonal_decompose
# series = pd.read_csv('airline-passengers.csv', header=0, index_col=0)
series = pd.read_csv('airline-passengers.csv', header=0, 
                     parse_dates=[0], index_col=[0])
# series.index.freq = 'MS' # To avoid that statsmodels try to guess frequency
print(f'Data types\n{series.dtypes}')

print(series.head())
result = seasonal_decompose(series, model='multiplicative')
result.plot()
pyplot.show()

result = seasonal_decompose(series, model='additive')
result.plot()
pyplot.show()


# df = pd.read_csv('retail_sales_used_car_dealers_us_1992_2020.csv', 
#                  header=0, infer_datetime_format=True, 
#                  parse_dates=[0], index_col=[0])


