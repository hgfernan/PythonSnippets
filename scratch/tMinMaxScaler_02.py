#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sample code for `sklearn`'s class `MinMaxScaler`
Created on Mon Dec  9 19:56:51 2024

@author: Sklearn official documentation
From: 
* https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html
"""
import numpy as np

from sklearn.preprocessing import MinMaxScaler

data = [[-1, 2], [-0.5, 6], [0, 10], [1, 18]]
print(f'data\n\t{data}')

scaler = MinMaxScaler(feature_range=(-1, 1))

print(f'scaler.fit(data)\n\t{scaler.fit(data)}')
# MinMaxScaler()

print(dir(scaler))

print(f'scaler.data_max_\n\t{scaler.data_max_}')

print(f'scaler.data_max_\n\t{scaler.data_min_}')

print(f'scaler.data_range_\n\t{scaler.data_range_}')

print(f'scaler.n_features_in_\n\t{scaler.n_features_in_}')

print(f'scaler.n_samples_seen_\n\t{scaler.n_samples_seen_}')

new_data = scaler.transform(data)
print(f'scaler.transform(data)\n{new_data}')
# [[-1.  -1. ]
# [-0.5 -0.5]
# [ 0.   0. ]
# [ 1.   1. ]]

old_data = scaler.inverse_transform(new_data)
print(f'scaler.inverse_transform(new_data)\n{old_data}')

# OBS Didn't understand it... 

arr = [[2, 2]]
print(f'arr\n\t{arr}')
print(scaler.transform(arr))
# [[1.5 0. ]]

arr = np.array([10, 11]).reshape(1, -1)
# arr = np.array([10, 11])
print(f'arr\n\t{arr}')
print(scaler.transform(arr))

