#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A testbed for tests -- 1st attempt, with `pytest`
Created on Mon Nov 11 22:06:44 2024

@author: pytest documentation team
From: 
* https://docs.pytest.org/en/stable/reference/fixtures.html
"""

import pytest

@pytest.fixture
def order():
    return []

@pytest.fixture
def outer(order, inner):
    order.append("outer")


class TestOne:
    @pytest.fixture
    def inner(self, order):
        order.append("one")

    def test_order(self, order, outer):
        assert type(order).__name__ == 'a'
        
        assert order == ["one", "outer"]

class TestTwo:
    @pytest.fixture
    def inner(self, order):
        order.append("two")

    def test_order(self, order, outer):
        assert order == ["two", "outer"]
    