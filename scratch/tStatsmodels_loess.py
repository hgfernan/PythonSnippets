#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sample code to decompose series using `statsmodels` STL 
Created on Mon Oct 28 21:24:18 2024

@author: hilton
"""

from statsmodels.datasets import co2
from statsmodels.tsa.seasonal import STL

import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()
data = co2.load().data
# data = co2.load(True).data
data = data.resample('ME').mean().ffill()

print(f'type(data) {type(data)}')

res = STL(data).fit()
res.plot()
plt.show()

print(f'data\n{data}')