#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sample code for `sklearn`'s class `MinMaxScaler`
Created on Mon Dec  9 19:56:51 2024

@author: Sklearn official documentation
From: 
* https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html
"""
import numpy as np

from sklearn.preprocessing import MinMaxScaler

data = [[-1, 2], [-0.5, 6], [0, 10], [1, 18]]
print(f'data\n\t{data}')

scaler = MinMaxScaler()

print(f'scaler.fit(data)\n\t{scaler.fit(data)}')
# MinMaxScaler()

print(f'scaler.data_max_\n\t{scaler.data_max_}')
# [ 1. 18.]

print(f'scaler.data_max_\n\t{scaler.data_min_}')
# [ 1. 18.]

print(f'scaler.data_range_\n\t{scaler.data_range_}')
# [ 1. 18.]

print(f'scaler.n_features_in_\n\t{scaler.n_features_in_}')
# [ 1. 18.]
print(f'scaler.n_samples_seen_\n\t{scaler.n_samples_seen_}')
# [ 1. 18.]

print(f'scaler.transform(data)\n\t{scaler.transform(data)}')
# [[0.   0.  ]
 # [0.25 0.25]
 # [0.5  0.5 ]
 # [1.   1.  ]]

arr = [[2, 2]]
print(f'arr\n\t{arr}')
print(scaler.transform(arr))
# [[1.5 0. ]]

arr = np.array([10, 11]).reshape(1, -11)
# arr = np.array([10, 11])
print(f'arr\n\t{arr}')
print(scaler.transform(arr))
