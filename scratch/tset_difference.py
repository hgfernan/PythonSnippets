#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 20 21:10:28 2024

@author: hilton
"""

d1 = ['__abstractmethods__', '__class__', '__delattr__', '__dict__', '__dir__', 
      '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', 
      '__getstate__', '__gt__', '__hash__', '__init__', '__init_subclass__', 
      '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', 
      '__reduce_ex__', '__repr__', '__setattr__', '__setstate__', '__sizeof__', 
      '__sklearn_clone__', '__str__', '__subclasshook__', '__weakref__', 
      '_abc_impl', '_build_request_for_signature', '_check_exog', 
      '_check_feature_names', '_check_n_features', '_doc_link_module', 
      '_doc_link_template', '_doc_link_url_param_generator', '_fit', 
      '_get_default_requests', '_get_doc_link', '_get_metadata_request', 
      '_get_param_names', '_get_tags', '_more_tags', '_repr_html_', 
      '_repr_html_inner', '_repr_mimebundle_', '_validate_data', 
      '_validate_params', '_warn_for_older_version', 'aic', 'aicc', 
      'arima_res_', 'arparams', 'arroots', 'bic', 'bse', 'conf_int', 
      'df_model', 'df_resid', 'endog_index_', 'fit', 'fit_predict', 
      'fit_with_exog_', 'fittedvalues', 'get_metadata_routing', 'get_params', 
      'hqic', 'maparams', 'maroots', 'maxiter', 'method', 'nobs_', 'oob', 
      'oob_', 'oob_preds_', 'order', 'out_of_sample_size', 'params', 
      'pkg_version_', 'plot_diagnostics', 'predict', 'predict_in_sample', 
      'pvalues', 'resid', 'sarimax_kwargs', 'scoring', 'scoring_args', 
      'seasonal_order', 'set_params', 'set_predict_request', 'start_params', 
      'summary', 'suppress_warnings', 'to_dict', 'trend', 'update', 
      'with_intercept']

d2 = ['__add__', '__class__', '__class_getitem__', '__contains__', 
      '__delattr__', '__dir__', '__doc__', '__eq__', '__format__', 
      '__ge__', '__getattribute__', '__getitem__', '__getnewargs__', 
      '__getstate__', '__gt__', '__hash__', '__init__', '__init_subclass__', 
      '__iter__', '__le__', '__len__', '__lt__', '__mul__', '__ne__', 
      '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__rmul__', 
      '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'count', 
      'index']

sd1 = set(d1)
sd2 = set(d2)

print(f'difference from 1 to 2\n{sd1 - sd2}')
print()
print(f'difference from 2 to 1\n{sd2 - sd1}')
      
