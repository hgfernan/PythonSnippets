#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Testing the use of the module subprocess
Created on Fri Feb  7 20:00:11 2025

@author: hilton
From:
* https://stackoverflow.com/questions/4514751/pipe-subprocess-standard-output-to-a-variable
"""

import subprocess # class CalledProcessError, run()
import signal # Signal

try:
    proc = subprocess.run(['../../cppstudy/scratch/bin/goodbye_cruel_world'], 
    # proc = subprocess.run(['ls', '-l'], 
                        stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE, 
                        check=True
                       )
    print(type(proc))
    print(proc.stderr)
    print(proc.stdout.decode(encoding='utf-8'))
    
except subprocess.CalledProcessError as exc:
    print(f'{type(exc).__name__}: {str(exc)}')
    print(dir(exc))

    print(f'exc.returncode {exc.returncode}')    
    print(exc.output)    
    print(exc.__dict__)
    
    print(exc.stdout)
    print(signal.Signals(-exc.returncode).name)
    
