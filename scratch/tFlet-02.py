#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Starting the adaptation of the code to show a table in Flet framework
Created on Mon Oct 14 10:23:16 2024

@author: hilton
From: 
* https://flet.dev/docs/controls/datatable/#examples
"""

import flet as ft

def main(page: ft.Page):
    page.add(
        ft.DataTable(
            columns=[
                ft.DataColumn(ft.Text("detail_id")),
                ft.DataColumn(ft.Text("occurrence")),
                ft.DataColumn(ft.Text("money_out"), numeric=True),
                ft.DataColumn(ft.Text("account_id"), numeric=True),
                ft.DataColumn(ft.Text("value"), numeric=True),
                ft.DataColumn(ft.Text("description"))
            ],
            rows=[
                # 37	2024-10-03	1	3	17.5	Pastel + Caldo de cana
                ft.DataRow(
                    cells=[
                        ft.DataCell(ft.Text("37")),
                        ft.DataCell(ft.Text("2024-10-03")),
                        ft.DataCell(ft.Text("1")),
                        ft.DataCell(ft.Text("3")),
                        ft.DataCell(ft.Text("17.50")),
                        ft.DataCell(ft.Text("Pastel + Caldo de cana")),
                    ],
                ),
                # 38	2024-10-04	1	1	20	Lunch
                ft.DataRow(
                    cells=[
                        ft.DataCell(ft.Text("38")),
                        ft.DataCell(ft.Text("2024-10-04")),
                        ft.DataCell(ft.Text("1")),
                        ft.DataCell(ft.Text("1")),
                        ft.DataCell(ft.Text("20")),
                        ft.DataCell(ft.Text("Lunch")),
                    ],
                ),
                # 39	2024-10-13	1	3	75.88	Lunch                
                ft.DataRow(
                    cells=[
                        ft.DataCell(ft.Text("39")),
                        ft.DataCell(ft.Text("2024-10-04")),
                        ft.DataCell(ft.Text("1")),
                        ft.DataCell(ft.Text("3")),
                        ft.DataCell(ft.Text("75.88")),
                        ft.DataCell(ft.Text("Supermarket Gold Prime")),
                    ],
                ),
            ],
        ),
    )

ft.app(main)