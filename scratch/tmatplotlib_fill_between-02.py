#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Tiny change to minimal sample code for `matplotlib.pyplot.fill_between()`
Created on Mon Nov 18 14:26:57 2024

@author: MatPlotLib documentation site
From: 
* https://matplotlib.org/stable/gallery/lines_bars_and_markers/fill_between_demo.html
"""
import numpy as np
from matplotlib import pyplot as plt

N = 21
x = np.linspace(0, 10, 11)
y = [3.9, 4.4, 10.8, 10.3, 11.2, 13.1, 14.1,  9.9, 13.9, 15.1, 12.5]
y = np.array(y)

# fit a linear curve and estimate its y-values and their error.
a, b = np.polyfit(x, y, deg=1)
y_est = a * x + b
# y_err = x.std() * np.sqrt(1/len(x) +
#                           (x - x.mean())**2 / np.sum((x - x.mean())**2))

resid = y - y_est
sse = np.sum(np.inner(resid, resid)) / (y.shape[0] - 1)

# y_err = np.full((len(y), 1), y.std())
# y_below = (y - y_err).tolist()
# y_below = y_below[0]
# y_above = (y + y_err).tolist()
# y_above = y_above[0]

y_err = np.full((len(y), 1), np.sqrt(sse))
y_below = (y_est - y_err).tolist()
y_below = y_below[0]
y_above = (y_est + y_err).tolist()
y_above = y_above[0]

print(y_below)

fig, ax = plt.subplots()
ax.plot(x, y_est, '-')
ax.fill_between(x, y_below, y_above, alpha=0.2)
ax.plot(x, y, 'o', color='tab:brown')
