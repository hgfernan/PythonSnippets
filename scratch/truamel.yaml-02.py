#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Loading configuration using module `ruamel.yaml`
Created on Wed Dec 18 17:11:31 2024

@author: hilton
"""

import sys # argv, exit()

import json
from ruamel.yaml import YAML

yaml_str = """\
sarima:
  - type: random
  - - n_iter: 90
    - order:
      - p:
          min: 0
          max: 10
      - d:
          min: 0
          max: 3
      - q:
          min: 0
          max: 10
    - seasonal_order:
      - P:
          min: 0
          max: 3
      - D:
          min: 0
          max: 3
      - Q:
          min: 0
          max: 10
      - s: 12          
"""

def main() -> int:
    yaml = YAML() 
    data = yaml.load(yaml_str)
    
    print(f'data:\n{data}')
           
    s = data['sarima']
    print(f'\ndata[\'sarima\']\n{s}')
    
    t = s[0]
    print(f'\ndata[\'sarima\'][0]\n{t}')
    
    n = s[1]
    print(f'\ndata[\'sarima\'][1]\n{n}')
    
    print('\nJSON')
    print(json.dumps(data, indent=2))
    
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main())

