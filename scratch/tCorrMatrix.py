#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sample code for creating a correlation matrix, and plotting it

Created on Sun Mar 24 21:57:47 2024

@author: user
From: 
* https://datatofish.com/correlation-matrix-pandas/
"""

import pandas as pd
import seaborn as sn
import matplotlib.pyplot as plt

data = {'A': [45, 37, 42, 35, 39],
        'B': [38, 31, 26, 28, 33],
        'C': [10, 15, 17, 21, 12]
        }

df = pd.DataFrame(data)

corr_matrix = df.corr()

print(corr_matrix)
print(corr_matrix.columns)

print(corr_matrix.dtypes)

fig,ax = plt.subplots() 

sn.heatmap(corr_matrix, annot=True, ax=ax)
plt.show()

r2 = corr_matrix.map(lambda x: x * x)
print(r2)

sn.heatmap(r2, annot=True)
plt.show()
