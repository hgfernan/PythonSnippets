#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Playing with `repr()`
Created on Sat Nov  9 17:10:03 2024

@author: hilton
"""

import sys       # argv, exit()

class WithoutDataclass:
    """
    A class with excess parameters to generate the pylint warning about
    excess parameters
    """
    def __init__(self, f0, f1, f2, f3, f4, f5, f6, f7, f8, f9):
        self.f0 = f0
        self.f1 = f1
        self.f2 = f2
        self.f3 = f3
        self.f4 = f4
        self.f5 = f5
        self.f6 = f6
        self.f7 = f7
        self.f8 = f8
        self.f9 = f9

    def __eq__(self, o): 
        for key in self.__dict__.keys():
            if self.__dict__[key] != o.__dict__[key]:
                return False

        return True 
        
    def __repr__(self) -> str:
        result : str = ''

        result += type(self).__name__ + '('
        result += str(self)
        result += ')'

        # Normal function termination
        return result


    def __str__(self) -> str:
        result : str = ''

        result += f'{self.f0}, {self.f1}, {self.f2}, {self.f3}, {self.f4}, '
        result += f'{self.f5}, {self.f6}, {self.f7}, {self.f8}, {self.f9}'

        # Normal function termination
        return result


def main() -> int:
    wo = WithoutDataclass(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
    print(f'wo\n{wo}')
    
    print(f'\nrepr(wo)\n{repr(wo)}')
    
    print(f'\nwo.__repr__(wo)\n{wo.__repr__()})')
    
    print(f'\neval(repr(wo))\n{eval (repr(wo) )}')
    
    print(f'\nwo == eval(repr(wo))\n{ wo == eval(repr(wo)) }')

    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main())
