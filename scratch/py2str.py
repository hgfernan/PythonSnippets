#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Convert a Python code to a Python string 
Created on Tue Sep 24 19:43:18 2024

@author: hilton
"""

import sys # argv, exit()

from typing import List

def bld_out_name(inp_name : str) -> str:
    if len(inp_name) == 0: 
        return 'python-out.py'
    
    # HINT handle the case when file name has more than one '.'    
    fields = inp_name.split('.')
    result : str = '.'.join(fields[ : -1] )+ '-out.py'
    
    # Normal function termination
    return result
    

def py_to_str(inp_name : str) -> bool:
    out_name : str = bld_out_name(inp_name)
    if inp_name == '':
        inp_f = sys.stdin
    else: 
        try:
            inp_f = open(inp_name, 'r')
            
        except Exception as exc:
            msg : str = f'\n{sys.argv[0]} ERROR Could not open file '
            msg  += f'\'{inp_name}\' for input\n'
            msg  += f'\t{type(exc).__name__}: {str(exc)}'
            print(msg, flush=True)
            
            # Return to indicate failure
            return False
        
        # HINT handle the case when file name has more than one '.'
        out_name = bld_out_name(inp_name)
        
    try:
        out_f = open(out_name, 'w')
                
    except Exception as exc:
        msg : str = f'\n{sys.argv[0]} ERROR Could not open file '
        msg  += f'\'{out_name}\' for output\n'
        msg  += f'\t{type(exc).__name__}: {str(exc)}'
        print(msg, flush=True)
        
        # Return to indicate failure
        return False
    
    # print(f'out_name {out_name}, out_f {out_f}')
    print('text = """', file=out_f, flush=True)
    for line in inp_f:
        out_line : str = line.rstrip()
        fields = out_line.split('"""')
        
        out_line = '\\"\\"\\"'.join(fields)
        
        print('\t' + out_line, file=out_f, flush=True)
            
    print('"""', file=out_f, flush=True)

    out_f.close()        
    
    # Normal function termination
    return True 

def main(argv : List[str]) -> int:
    inp_name : str = ""
    if len(argv) > 1:
        inp_name = argv[1]
        
        # TODO handle multiple input files
        
    if not py_to_str(inp_name): 
        msg : str = f'{argv[0]} *** ERROR The program will be terminated ***'
        print(msg, flush=True)
        
        # Return to indicate failure
        return False
    
    print(f'{argv[0]}: Output file created as \'{bld_out_name(inp_name)}\'', 
          flush=True)
    
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))

