#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 24 18:11:40 2024

@author: Streamlit site creators 
From: 
* https://docs.streamlit.io/develop/api-reference/layout/st.empty
"""

import streamlit as st
import time # sleep()

def inside_container(internal):
    with internal.container(): 
        st.write('Testing inside container')
    

placeholder = st.empty()
internal = st.empty()

# Replace the placeholder with some text:
with placeholder.container():
    st.write("Hello")
    
    inside_container(internal)

time.sleep(3)

# Replace the text with a chart:
with placeholder.container():
    placeholder.line_chart({"data": [1, 5, 2, 6]})
    
    inside_container(internal)

time.sleep(3)

# Replace the chart with several elements:
with placeholder.container():
    st.write("This is one element")
    st.write("This is another")
    
    inside_container(internal)

time.sleep(3)

# Clear all those elements:
placeholder.empty()
internal.empty()