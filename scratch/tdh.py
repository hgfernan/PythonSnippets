#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Implementation and some testing of double hash annotations in code 
templates
Created on Wed Feb 26 13:19:42 2025

@author: hilton
"""

import re  # compile(), search()
import sys # argv, exit

from typing import Any, List, Union

class DhLineParser:
    """
    Will parse a string with double hash markings
    """
    re_int : str = r'[\-\+]{0,1}(\d)+'
    re_fixed : str = r'[\-\+]{0,1}(\d)+\.{0,1}(\d)+'
    re_sci : str = r'[\-\+]{0,1}(\d)+\.{0,1}(\d)+[eE][\-\+]{0,1}(\d){1,4}'
    
    p_int = re.compile(re_int)
    p_fixed = re.compile(re_fixed)
    p_sci = re.compile(re_sci)

    def __init__(self,  line : str, dbl_quote : bool = True) -> None:
        self.items : List[str] = line.split('##')
        self.n_blanks = len(self.items) // 2

        self.blanks : List[int] = []
        for ind in range(self.n_blanks):
            self.blanks.append(2*ind + 1)
        
        self.dbl_quote = dbl_quote
        self.quotechar = '\''
        if self.dbl_quote:
            self.quotechar = '"'

    def get_n_blanks(self) -> int:
        """
        Obtain the number of fields that can be replaced

        Returns
        -------
        int
            Number of fields open to blank.

        """
        return self.n_blanks

    def get_blanks(self) -> List[str]:
        return self.blanks

    def get_blank(self, ind) -> int:
        if (ind < 0) or (ind >= self.n_blanks):
            return None

        return self.items[self.blanks[ind]]

    def get_n_items(self) -> int:
        return len(self.items)

    def get_items(self) -> int:
        return self.items

    def get_item(self, ind) -> int:
        if (ind < 0) or (ind >= self.get_n_items()):
            return None

        return self.items[ind]

    def fill_in(self, ind, elmt : Union[float, str]) -> Any:
        if (ind < 0) or (ind >= self.n_blanks):
            return None

        abs_ind = self.blanks[ind]        
        result : Union[float, str] = self.items[abs_ind]

        if type(elmt) != str:
            self.items[abs_ind] = elmt
            
            # Normal function termination
            return result
        
        rv = __class__.p_int.search(elmt)
        if rv:
            span : List[int] = rv.span()
            frag : str = elmt[span[0] : span[1]]
            # print(self.blanks)
            # print(self.blanks, ind, int(frag))
            self.items[abs_ind] = int(frag)
            
            # Normal function termination
            return result
        
        rv = __class__.p_sci.search(elmt)
        if rv: 
            rv = __class__.p_float.search(elmt)
            
        if rv:
            span = rv.span()
            frag : str = elmt[span[0] : span[1]]
            self.items[abs_ind] = float(frag)
            
            # Normal function termination
            return result
        
        # HINT it's not a numeric value, let's quote it        
        self.items[abs_ind] = self.quotechar + elmt + self.quotechar
            
        # Normal function termination
        return result
    
    def rebuild(self) -> str:
        result : str = ''
        
        for ind in range(len(self.items)):
            result += str(self.items[ind])
            
        # Normal function termination
        return result

def find_blank(line : str) -> None:
    """
    Find fields that should be replaced, and print results

    Parameters
    ----------
    line : str
        DESCRIPTION.

    Returns
    -------
    None
        DESCRIPTION.

    """
    print(f'input: {line}')
    fields : List[str] = line.split('##')
    print(fields)

    n_items = len(fields) // 2
    for ind in range(n_items):
        blank = fields[2*ind + 1]
        print(f'blank {blank}')

    print()
    
def test_dhlineparser(line : str, fillings : List[str]) -> str:
    """
    Test class DhLineParser

    Parameters
    ----------
    base : str
        DESCRIPTION.
    fillings : List[str]
        DESCRIPTION.

    Returns
    -------
    str
        DESCRIPTION.

    """
    print(f'Will fill in the blanks in \n\t\'{line}\'')
    dh_parser = DhLineParser(line)
    
    print(f'With the fillings\n\t{fillings}')
    
    n_items : int = dh_parser.get_n_items()
    print(f'n_items {n_items}')
    
    for ind in range(n_items):
        print(f'\t{ind:3} \'{dh_parser.get_item(ind)}\'')
    
    n_blanks : int = dh_parser.get_n_blanks()
    print(f'n_blanks {n_blanks}')
    
    for ind in range(n_blanks):
        print(f'\t{ind:3} \'{dh_parser.get_blank(ind)}\'')
        
    print('Will replace')
    for ind in range(len(fillings)):
        old = dh_parser.fill_in(ind, fillings[ind])
        new = dh_parser.get_blank(ind)
        print(f'\t{ind} Replaced \'{old}\' with {new}')
        
    print('After replacement, the fillings are')
    for ind in range(min(len(fillings), dh_parser.get_n_blanks()) ):
        rv = dh_parser.get_blank(ind)
        print(f'\t{ind} {rv}')
    
    print('Will rebuild the replaced string')
    print(f'\t{dh_parser.rebuild()}')
    print()

def main(argv : List[str]) -> int:
    """
    The main program, tiny test of class DhLineParser

    Parameters
    ----------
    argv : List[str]
        Command line parameters.

    Returns
    -------
    int
        0 if everything fine, an error code otherwise.

    """
    # find_blank('aaaa##b##aba')
    # find_blank('aaaa##b##aba##x##')
    # find_blank('aaaa##b##aba##x##aa')
    # find_blank('##b##aba##x##')

    # line : str = 'aaaa##b##aba'
    # print(f'Will fill the blanks in \n\t\'{line}\'')
    # dh_parser = DhLineParser(line)
    
    # n_items : int = dh_parser.get_n_items()
    # print(f'n_items {n_items}')
    
    # for ind in range(n_items):
    #     print(f'\t{ind:3} \'{dh_parser.get_item(ind)}\'')

    # dh_parser = DhLineParser('aaaa##b##aba')
    
    # n_blanks : int = dh_parser.get_n_blanks()
    # print(f'n_blanks {n_blanks}')
    
    # for ind in range(n_blanks):
    #     print(f'\t{ind:3} \'{dh_parser.get_blank(ind)}\'')
        
    # print('Will replace')
    # values : List[str] = ['122']
    # for ind in range(len(values)):
    #     old = dh_parser.fill_in(ind, values[ind])
    #     new = dh_parser.get_blank(ind)
    #     print(f'\t{ind} Replaced \'{old}\' with {new}')
        
    # print('After replacement')
    # for ind in range(len(values)):
    #     rv = dh_parser.get_blank(ind)
    #     print(f'\t{ind} {rv}')
    
    # print('Will rebuild the replaced string')
    # print(f'\t{dh_parser.rebuild()}')
    
    test_dhlineparser('aaaa##b##aba', ['122'])
    test_dhlineparser('aaaa##b##aba##x##', ['122', 'hjelsberg'])
    test_dhlineparser('aaaa##b##aba##x##', ['122', 'hjelsberg', 'karma'])
    
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
