#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generation of beautiful delta, ie numbers easy to understand

Created on Tue Mar 19 20:57:00 2024

@author: user
"""

import sys    # exit()
import math   # log10(), 
import random # srand(), rand(), randint()

from typing import List
from types  import SimpleNamespace

def create_classes(x_min : int, x_max: int) -> SimpleNamespace: 
    b_delta : List[int] = [5, 10, 25, 125]
    b_multi : List[int] = [10, 1, 10, 1]
    n_classes : int = 10
    
    if x_min > x_max: 
        tmp : int = x_min
        
        x_min = x_max
        x_max = tmp
        
    print(f'\nx_min {x_min:,}, x_max {x_max:,}')

    delta : float = (x_max - x_min) / n_classes
    iexp  : int = int(math.log10(delta))
    divider : float = math.pow(10, iexp)
    
    print(f'delta {delta:,}, iexp {iexp}, divider {divider}')
    
    min_diff : int = 1e12
    min_ind : int = -1

    n_divider = divider
    for ind in range(len(b_delta)):
        n_delta = b_delta[ind]
        n_divider *= n_delta
        n_min = math.floor(x_min / n_divider)*n_divider
        n_max = math.ceil(x_max / n_divider)*n_divider
        
        diff = abs(x_min - n_min) + abs(x_max - n_max)
        
        print(f'\nn_min {n_min}, n_max {n_max}')
        print(f'diff {diff}, n_delta {n_delta}, n_divider {n_divider}')
        print(f'n_divider {n_divider} {int( math.log10(n_divider)) }')
        
        if diff < min_diff: 
            min_diff = diff
            min_ind = ind
            min_n_min = n_min
            min_n_max = n_max
            min_n_divider = n_divider
        
        n_divider /= b_multi[ind]
        
    result : SimpleNamespace = SimpleNamespace()
    result.n_min = min_n_min
    result.n_max = min_n_max
    result.delta = (result.n_max - result.n_min) / n_classes
    result.n_divider = min_n_divider
    result.n_classes = math.ceil(result.delta / result.n_divider)
    
    # Normal function termination
    return result


def main():
    tmp: int = 0
    
    # lower : int = 559188684
    # upper : int = 2509631080
    
    # rv : SimpleNamespace = create_classes(upper, lower)
    # print(rv)
    
    # return 0

    beautiful = [1.0, 1.25, 1.5, 2.0, 2.5, 3.0, 4.0, 5.0, 6.0, 7.0, 
        8.0, 9.0, 999999]
    # beautiful = [ (n + 1)*0.25 for n in range(40) ] + [9999]
    min_num : int = 0
    max_num : int = (1 << 32) - 1
    
    min_classes = 15
    max_classes = 27
    
    n_runs : int  = 12
    
    print(f'max_num {max_num}, min_num {min_num}')
    
    random.seed((1 << 23) - 1)

    print(beautiful)
    
    # return 0
    
    for run in range(n_runs):
        upper = random.randint(min_num, max_num)    
        lower = random.randint(min_num, max_num)
        
        if upper < lower:
            tmp = upper
            upper = lower
            lower = tmp
                 
        n_classes = random.randint(min_classes, max_classes)
        
        my_floor = math.floor if lower > 0.0 else math.ceil
        my_ceil  = math.ceil  if upper > 0.0 else math.floor
        # my_ceil  = math.ceil
        
        delta = float(upper - lower) / (n_classes - 1)
        
        iexp = int(math.log10(delta))
        divider = 10**iexp
        ratio = delta / divider
        
        fmt = f'n_classes {n_classes}, interval [{lower}, {upper}), '
        fmt += f'delta {delta}'
        print(fmt)
        
        ind = -1
        while ratio > beautiful[ind + 1]:
            ind += 1
        
        fmt = f'\tratio {ratio}, smaller: {ind} {beautiful[ind]}'
        print(fmt)
        
        new_delta = beautiful[ind]*divider
        
        new_lower = my_floor(lower / new_delta)*new_delta
        new_upper = my_ceil (upper / new_delta)*new_delta
        
        new_n_classes = int((new_upper - new_lower) / new_delta) + 1
        
        fmt = f'\tnew: delta {new_delta}, [{new_lower}, {new_upper})'
        print(fmt)
        
        fmt = f'\t     n_classes {new_n_classes}'
        print(fmt)
    
        # TODO confirm new_delta against new_lower and new_upper
        
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main())
