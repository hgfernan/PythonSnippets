#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Adapting Wikipedia's binary search algo to inexact binary search idea
Created on Wed Dec  4 21:10:36 2024

@author: hilton
"""

import sys  # argv, exit()
import math # floor()

from typing import List

import numpy  as np # submodule random

# function binary_search(A, n, T) is
#     L := 0
#     R := n − 1
#     while L ≤ R do
#         m := floor((L + R) / 2)
#         if A[m] < T then
#             L := m + 1
#         else if A[m] > T then
#             R := m − 1
#         else:
#             return m
#     return unsuccessful


def binary_search(x : float, arr : List[str]) -> int:
    l : int = 0
    r : int = len(arr) - 1
    
    while l <= r:
        m = math.floor((l + r) / 2)
        if arr[m] < x:
            l = m + 1 
        elif arr[m] > x:
            r = m - 1
        else: 
            return m
        
    # Normal function termination
    return -1


def minimum_search(arr : List[str]) -> int:
    l : int = 0
    r : int = len(arr) - 1
    
    while (r - l) > 1:
        m = math.floor((l + r) / 2)
        
        if arr[l] < arr[m]:
            r = m
        elif arr[m] > arr[r]:
            l = m

    if arr[l] < arr[m]:
        return l

    if arr[m] > arr[r]:
        return r
    
    # Normal function termination
    return m 
    
def main(args : List[str]) -> int:
    # arr : List[float] = [10., 15.0, 50., 60.]     
    # print(f'Considering arr\n\t{arr}\n')
    
    # for x in [5.0, 10.0, 13.0, 14.0]:
    #     print(f'Looking for {x}')
        
    #     ind = binary_search(x, arr)
    #     if ind != -1:
    #         print(f'\t{x} found in position {ind}')
    #     else:
    #         print(f'\t{x} was not found')
    
    # print(f'\nminimum_search(arr): {minimum_search(arr)}')
    
    arr2 : List[float] = [60., 50.0, 15., 10.] 
    print(f'\nConsidering arr2\n\t{arr2}\n')
   
    print(f'minimum_search(arr2): {minimum_search(arr2)}')

    rng = np.random.default_rng((1 << 23) - 1)
    arr3 = rng.random(size=11)
    
    print(f'\nConsidering arr3\n\t{arr3}\n')
    print(f'\tMinimum {arr3.min()}')
   
    ind = minimum_search(arr3)
    print(f'minimum_search(arr3): {ind}')
    print(f'\tValue {arr3[ind]}')
    
    print('\nReordering arr3 as arr4')
    arr4 = sorted(arr3)
    
    print(f'\nConsidering arr4\n\t{arr4}\n')
    print(f'\tMinimum {min(arr4)}')
   
    ind = minimum_search(arr4)
    print(f'minimum_search(arr4): {ind}')
    print(f'\tValue {arr4[ind]}')
    
    print('\nReordering arr3 reversely as arr5')
    arr5 = sorted(arr3, reverse=True)
   
    ind = minimum_search(arr5)
    print(f'minimum_search(arr5): {ind}')
    print(f'\tValue {arr5[ind]}')
        
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
