#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Another baby step in adapting the Flet table demo code
Created on Mon Oct 14 10:23:16 2024

@author: hilton
From: 
* https://flet.dev/docs/controls/datatable/#examples
"""

import flet as ft

from typing import List

def main(page: ft.Page):
    data : List[List[str]] = [
        ["37", "2024-10-03", "1", "3", "17.5", " Pastel + Caldo de cana"],
        ["38", "2024-10-04", "1", "1", "20", "   Lunch"],
        ["39", "2024-10-13", "1", "3", "75.88", "Supermarket Gold Prime"]
    ]
    columns : List[ft.DataColumn] = [ 
        ft.DataColumn(ft.Text("detail_id")),
        ft.DataColumn(ft.Text("occurrence")),
        ft.DataColumn(ft.Text("money_out"), numeric=True),
        ft.DataColumn(ft.Text("account_id"), numeric=True),
        ft.DataColumn(ft.Text("value"), numeric=True),
        ft.DataColumn(ft.Text("description"))
    ]
    
    rows : List[List[ft.DataRow]] = []
    
    for ind in range(len(data)):
        cells : List[ft.DataCell] = []
        for ind2 in range(len(data[ind])):
            cell : ft.DataCell = ft.DataCell( ft.Text(data[ind][ind2]) )
            cells.append(cell)
            
        rows.append(ft.DataRow(cells))
    
    page.add(
        ft.DataTable(
            columns=columns,
            rows=rows,
        ),
    )

ft.app(main)
