#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
How to detect more than one use of an option when using module `argparse`
Created on Thu Dec 19 11:13:18 2024

@author: @tmrlvi in stackoverflow.com 
From: 
* https://stackoverflow.com/questions/23032514/argparse-disable-same-argument-occurrences/23032953#23032953
"""
import sys      # argv
import argparse # class Action, class ArgumentParser

class UniqueStore(argparse.Action):
    def __call__(self, parser, namespace, values, option_string):
        if getattr(namespace, self.dest, self.default) is not self.default:
            parser.error(option_string + " appears several times.")
        setattr(namespace, self.dest, values)

parser = argparse.ArgumentParser(exit_on_error=False)

print(dir(parser))

args = None
try:
    parser.add_argument('-f', '--foo', action=UniqueStore)
    args = parser.parse_args('-f a --foo b'.split())

# except argparse.ArgumentError as exc:
except Exception as exc:
    print('Catching an argumentError')
    print(f'\t{type(exc).__name__}: {str(exc)}')

print('\n' + str(args))

# cmdline = '1 --foo 2 -foo 3'
# print(f'Will parse the erroneous command line \'{cmdline}\'')
# args = parser.parse_args(cmdline.split())
