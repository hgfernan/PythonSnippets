#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 14:40:27 2024

@author: Sachin Date
From:
* https://timeseriesreasoning.com/contents/time-series-decomposition/
"""

import datetime
import pandas as pd
import numpy as np
import math
from matplotlib import pyplot as plt
 
 
mydateparser = lambda x: datetime.datetime.strptime(x, '%d-%m-%y')
 
# df = pd.read_csv('retail_sales_used_car_dealers_us_1992_2020.csv', 
#                  header=0, index_col=0, parse_dates=['DATE'], 
#                  date_parser=mydateparser)

df = pd.read_csv('retail_sales_used_car_dealers_us_1992_2020.csv', 
                 header=0, index_col=0, parse_dates=['DATE'], 
                 date_format='%d-%m-%y')

 
fig = plt.figure()
 
fig.suptitle('Retail sales of used car dealers in the US in millions of dollars')
 
df['Retail_Sales'].plot()