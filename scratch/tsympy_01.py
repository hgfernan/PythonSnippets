#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Some experiences with `sympy` handling of polynomials.
Created on Tue Nov 12 18:59:07 2024

@author: hilton
"""

import sympy
x, y = sympy.symbols('x y') 

cube = sympy.expand((x - y)**3)
print(f'expand((x - y)**3)\n{cube}') 

pol = sympy.poly((x - y)**3)
print(f'sympy.poly((x - y)**3)\n{pol}') 

pol = sympy.poly((x - 1)**3)
print(f'sympy.poly((x - 1)**3)\n{pol}') 

print(f'pol.all_coeffs()\n{pol.all_coeffs()}')

print(f'type(pol.all_coeffs())\n{ type(pol.all_coeffs()) }')