#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 23:02:41 2024

@author: 🌈 ☯️Teresita🐧👭 @linuxgal@techhub.social
"""

#!/usr/bin/python3
import sys
x=int(sys.argv[1])
anums = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
rnums = "M CM D CD C XC L XL X IX V IV I".split()
ret = []
for a,r in zip(anums, rnums):
    n,x = divmod(x,a)
    ret.append(r*n)
print (''.join(ret))