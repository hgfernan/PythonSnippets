#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A testbed for tests -- 2nd attempt, with `pytest`
Created on Tue Nov 12 21:56:55 2024

@author: hilton
"""
import pytest 
import sympy

@pytest.fixture
def coeffs():
    x, y = sympy.symbols('x y') 
    pol = sympy.poly((x - 1)**3)
    
    return pol.all_coeffs()

class TestOne:
    def test_01(self, coeffs):
        assert type(coeffs).__name__ == 'list'
        assert coeffs[0] == 1
        
    def test_02(self, coeffs):
        assert sum(coeffs) == 0.0

