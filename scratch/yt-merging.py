#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Starter for youtube downloading and merging
Created on Mon Dec 16 09:22:30 2024

@author: hilton
"""

import os   # chmod()
import sys  # exit() 
import stat # 

# import csv # QUOTE_NONNUMERIC

# import os.path 
# import subprocess 

# from typing import List

import pandas as pd # class DataFrame, read_csv()

# TODO for all not downloaded files, do 
    # TODO generate a video dowload line, from master table
    # TODO generate an audio dowload line, from master table
    # TODO save it as a script 
    # TODO mark the file as processed in the master table file

def main() -> int:
    # TODO read master table file 
    mstr_name : str = 'master.csv'
    try:
        with open(mstr_name, 'r'):
            pass
    except FileNotFoundError as exc:                
        print(f'{sys.argv[0]}: Could not \'open{mstr_name}\' file for input')
        print(f'\t{type(exc).__name__}: {str(exc)}') 
        print('*** The program will be terminated ***', flush=True)
        
        # Return to indicate failure
        return 1
        
    master = pd.read_csv(mstr_name, index_col=False)
    master = master.set_index('Seq')
    
    # print(master)
    
    # HINT for each entry in the URL table    
    for ind in range(len(master)):
        url        : str  = master.iloc[ind, 0]
        audio      : str  = master.iloc[ind, 1]
        video      : int  = master.iloc[ind, 2]
        dwn_script : str  = master.iloc[ind, 3]
        downloaded : bool = master.iloc[ind, 4]
        mrg_script : str  = master.iloc[ind, 5]
        merged     : bool = master.iloc[ind, 6]
        
        # print(master.iloc[ind])

        if downloaded: 
            print(f'URL \'{url}\' already downloaded')
            continue
        
        dwn_script = f'scripts/download_{master.index[ind]:02d}.sh'
        mrg_script = f'scripts/merge_{master.index[ind]:02d}.sh'
        print(f'dwn_script {dwn_script}')
        print(f'mrg_script {mrg_script}')
        
        dwn_f = open(dwn_script, 'w')
        line : str = ''
        line += '#! /bin/bash\n'
        line += '\n'
        line += 'function get_newest() {\n'
        line += '    newest=$( ls -ct | head -2 | tail -1 )\n'
        line += '    suff=$( echo "${newest}"  | awk '
        line +=        '\'BEGIN{FS="."}{ print $2 }\' )\n'
        line += '\n'
        line += '    if [[ "${suff}." == "description." ]]; then\n'
        line += '        newest=$( ls -ct | head -1 )\n'
        line += '    fi\n'
        line += '}\n'
        line += ''
        print(line, file=dwn_f, flush=True)

        # TODO generate an audio dowload line, from master table    
        print('cd audio', file=dwn_f, flush=True)
        
        # yt-dlp --format 244 -4 -k 'https://www.youtube.com/watch?v=ksRgqwyYb7s
        line = f'yt-dlp --write-description -4 -k --format {audio} '
        line += f'\'{url}\''
        print(line, file=dwn_f, flush=True)

        line = 'newest=""'
        print(line, file=dwn_f, flush=True)

        line = 'get_newest'
        print(line, file=dwn_f, flush=True)
        
        line = 'audio_f="${newest}"\n'
        print(line, file=dwn_f, flush=True)        

        line = 'base=$( echo "${audio_f}"  | '
        line += "awk 'BEGIN{FS="'"."}'
        line += "{ print $1 }' )"
        print(line, file=dwn_f, flush=True)
        
        line = 'audio_f=audio/"${audio_f}"'  
        print(line, file=dwn_f, flush=True)
    
        print('cd -\n', file=dwn_f, flush=True)
        
        # TODO generate a video dowload line, from master table
        print('cd video', file=dwn_f, flush=True)
        
        line = f'yt-dlp --write-description -4 -k --format {video} '
        line += f'\'{url}\''
        print(line, file=dwn_f, flush=True)

        line = 'newest=""'
        print(line, file=dwn_f, flush=True)

        line = 'get_newest'
        print(line, file=dwn_f, flush=True)
        
        line = 'video_f="${newest}"\n'
        print(line, file=dwn_f, flush=True)        

        line = 'video_f=video/"${video_f}"'  
        print(line, file=dwn_f, flush=True)
        
        print('cd -\n', file=dwn_f, flush=True)
    
        print('composite="${base}".mp4', file=dwn_f, flush=True)
        
        line = f"echo -e '#! /bin/sh\\n' > {mrg_script}" 
        print(line, file=dwn_f, flush=True)
        
        quotes = "'" + '"' + "'"
        line = 'echo '
        line += 'ffmpeg '
        line += '-i ' + quotes + "${audio_f}" + quotes + ' '
        line += '-i ' + quotes + "${video_f}" + quotes + ' '
        line += '-c:a aac '
        line += quotes + "${composite}" + quotes + ' ' 
        line += '>> ' + f'{mrg_script}' 
        print(line, file=dwn_f, flush=True)

        line = "echo -e '\\n' >> " + f'{mrg_script}'  
        print(line, file=dwn_f, flush=True)

        line = f'chmod u+x {mrg_script}'
        print(line, file=dwn_f, flush=True)
        
        dwn_f.close()
        
        statinfo = os.stat(dwn_script)
        print(f'\nBefore chgmod: statinfo\n{statinfo}')
        
        os.chmod(dwn_script, statinfo.st_mode | stat.S_IEXEC)
        print(f'\nAfter chgmod: statinfo\n{statinfo}')
                
        return 1
        
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main())    