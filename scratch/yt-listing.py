#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Starter for youtube listing of available formats
Created on Mon Dec 16 09:45:06 2024

@author: hilton
"""

import csv # QUOTE_NONNUMERIC
import sys # exit() 
import subprocess # run(), class CompleteProcess

from typing import List

import pandas as pd # class DataFrame, read_csv()



def main() -> int:
    # TODO read the URL table    
    tbl_name : str = 'formats.csv'
    try:
        with open(tbl_name, 'r'):
            pass
    except FileNotFoundError as exc:                
        print(f'{sys.argv[0]}: Could not open file \'{tbl_name}\' for input')
        print(f'\t{type(exc).__name__}: {str(exc)}') 
        print('*** The program will be terminated ***', flush=True)
        
        # Return to indicate failure
        return 1
    
    table = pd.read_csv(tbl_name, index_col=False)
    table = table.set_index('Seq')
    
    # TODO for each entry in the URL table
    for ind in range(len(table)):
        url    : str =  table.iloc[ind, 0]
        name   : str =  table.iloc[ind, 1]
        listed : int = table.iloc[ind, 2]
       
        # TODO if already listed go to the next entry
        if listed == 1:
            print(f'\'{url}\' already listed')
            continue
        
        # TODO run  yt-dlp to get the format list
        # TODO save the output in the URL table
        out_f = open('formats/' + name, 'w')
        params : List[str] = ['yt-dlp', '--list-formats', url]
        proc = subprocess.run(params, stdout=out_f)
                
        print(proc)
        
        if proc.returncode == 0:
            # TODO mark the entry as listed in the URL table            
            table.iloc[ind, 2] = 1
            
            # TODO save the URL table 
            table.to_csv(tbl_name, quoting=csv.QUOTE_NONNUMERIC)
        
    # Normal function termination
    return 0

if __name__ == '__main__': 
    sys.exit(main())

