#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Adding rows to a dataframe without column labels
Created on Wed Feb  5 16:24:15 2025

@author: hilton
From:
* https://www.geeksforgeeks.org/how-to-add-one-row-in-an-existing-pandas-dataframe/
"""

import pandas as pd
# data = {0: ["Alice", "Bob"], 1: [25, 30]}
# df = pd.DataFrame(data)
df = pd.DataFrame()

new_row = pd.DataFrame({0: ["Eve"], 1: [28]})
df = pd.concat([df, new_row], ignore_index=True)
print(df)