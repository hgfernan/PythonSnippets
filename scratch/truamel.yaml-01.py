#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Learning to use the module `ruamel.yaml`
Created on Wed Dec 18 15:17:25 2024

@author: hilton
From: 
* https://yaml.readthedocs.io/en/latest/example/
"""

import sys # stdout

from ruamel.yaml import YAML

yaml_str = """\
first_name: Art
occupation: Architect  # This is an occupation comment
about: Art Vandelay is a fictional character that George invents...
"""

yaml = YAML()
data = yaml.load(yaml_str)
data.insert(1, 'last name', 'Vandelay', comment="new key")
yaml.dump(data, sys.stdout)

# 
# 
# Expected output:
#    
# first_name: Art
# last name: Vandelay    # new key
# occupation: Architect  # This is an occupation comment
# about: Art Vandelay is a fictional character that George invents...
#    

print('\nMy attempt\n')

print('\nDictonary')
yaml_struct = [{'type':'random'}]
yaml.dump(yaml_struct, sys.stdout)

print('\nDictonary plus p under it')
yaml_struct = [{'type':'random'}, ['p']]
yaml.dump(yaml_struct, sys.stdout)

print('\nDictonary plus p under it, and limits under p\n')
yaml_struct = [{'sarima': [ 
                            {'type': 'random'}, 
                            [
                                {'n_iter' : 90},
                                {'order' : 
                                     [
                                         {'p' : {'min' : 0, 'max' : 10}},
                                         {'d' : {'min' : 0, 'max' : 3}},
                                         {'q' : {'min' : 0, 'max' : 10}},                        
                                     ]
                                },
                                {'seasonal_order' : 
                                     [
                                         {'P' : {'min' : 0, 'max' : 3}},
                                         {'D' : {'min' : 0, 'max' : 3}},
                                         {'Q' : {'min' : 0, 'max' : 10}},                        
                                         {'s' : 12},                        
                                     ]
                                }
                            ]
                          ]
                }
              ]
yaml.dump(yaml_struct, sys.stdout)

print('\nLoading from a string\n')

yaml_str = """\
- sarima:
  - type: random
  - - n_iter: 90
    - order:
      - p:
          min: 0
          max: 10
      - d:
          min: 0
          max: 3
      - q:
          min: 0
          max: 10
    - seasonal_order:
      - P:
          min: 0
          max: 3
      - D:
          min: 0
          max: 3
      - Q:
          min: 0
          max: 10
      - s: 12
"""

# yaml_str = """\
# model : SARIMA
# type: random
#   order
#     p
#       min : 0
#       max : 10
# """


data = yaml.load(yaml_str)
yaml.dump(data, sys.stdout)
