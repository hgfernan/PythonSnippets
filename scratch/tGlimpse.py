#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 24 23:41:23 2024

@author: user
"""

import pandas as pd

def glimpse(df: pd.DataFrame) -> pd.DataFrame:
    """
    Similar to R's glimpse()

    Parameters
    ----------
    df : pd.DataFrame

    Returns
    -------
    pd.DataFrame
    """
    print(f"Rows: {df.shape[0]}")
    print(f"Columns: {df.shape[1]}")

    sample_size = min(df.shape[0], 5)

    return (
        df.sample(sample_size)
        .T.assign(dtypes=df.dtypes)
        .loc[
            :, lambda x: sorted(x.columns, key=lambda col: 0 if col == "dtypes" else 1)
        ]
    )
df = pd.DataFrame({"column_one": ["A", "B", "C", "D"], "column_two": [1, 2, 3, 4]})

print(df.pipe(glimpse))

print('\n')
df.info()