#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Improving 1% per day

Created on Fri Oct  6 14:40:39 2023

@author: user
"""

orig = 100
total = orig
nom_increment = 0.01

for ind in range(1, 366): 
    increment = total * nom_increment 
    new_total = total + increment 
    
    print(f'{ind} prev {total:.2f} increment {increment:.2f} new {new_total}:.2f')
    
    total = new_total

improvement = (total - increment) / orig * 100

print(f'Improvement {improvement:.2f}\n')

total = orig

for ind in range(1, 366):
    increment = total * nom_increment 
    new_total = total - increment 
    
    print(f'{ind} prev {total:.2f} increment {increment:.2f} new {new_total}:.2f')
    
    total = new_total
    