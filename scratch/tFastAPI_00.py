#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 15 15:53:47 2024

@author: hilton
"""

from fastapi import FastAPI

app = FastAPI()

@app.get("/")
def read_root():
    return "Hello World!"
