#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Testbed for learning `pandas.read_csv()`'s `chunksize` parameter, 2nd take
Created on Mon Dec  2 17:21:19 2024

@author: Pandas documentation crew
From:
* https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html#io-chunking
"""

import numpy  as np
import pandas as pd 

df = pd.DataFrame(np.random.randn(10, 4))
df.columns = ['colA', 'colB', 'colC', 'colD']

df.to_csv("tmp.csv", index=False)

table = pd.read_csv("tmp.csv")
bypass = 3
n_records = len(table) - bypass

print(table)

with pd.read_csv("tmp.csv", skiprows=bypass, chunksize=n_records) as reader:
    print(reader)
    for chunk in reader:
        pass
        # print(type(chunk).__name__)
        # print(chunk)
    
    chunk.columns = df.columns        
    print(chunk)
