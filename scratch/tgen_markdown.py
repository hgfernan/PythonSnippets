#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generating Markdown file from simple Python structs
Created on Thu Nov 14 21:06:25 2024

@author: hilton
"""

import sys # argv, exit()

from typing import List

class GenMarkdown:
    """
    Generate a Markdown text
    """
    def __init__(self, out_f) -> None:
        """
        Constructor

        Parameters
        ----------
        out_f : a class with `write()` and `flush()` methods
            DESCRIPTION.

        Returns
        -------
        None
            DESCRIPTION.

        """
        self.out_f = out_f
        self.previous = False

    def write_header(self, hdr_text : str) -> None:
        """
        Write the document header

        Parameters
        ----------
        hdr_text : str
            Header text, without the hash char '#'.

        Returns
        -------
        None.

        """
        # TODO confirm that a header was written, and only one
        self.out_f.write('# ' + hdr_text + '\n')
        self.out_f.flush()

        self.previous = True

    def write_subheader(self, subhdr_text : str) -> None:
        """
        Write a subheader; i. e. a text prefixed with a double hash '#'

        Parameters
        ----------
        subhdr_text : str
            Subheader text, without the two hash chars '##'.

        Returns
        -------
        None

        """
        # TODO confirm there is no other subheader with the same name
        line : str = '\n' if self.previous else ''
        line += '## ' + subhdr_text + '\n'

        self.out_f.write(line)
        self.out_f.flush()

        self.previous = True

    def write_subsubheader(self, subhdr_text : str):
        """
        Write a subsubheader; i. e. a text prefixed with a triple hash '#'

        Parameters
        ----------
        subhdr_text : str
            Subheader text, without the hash char triplet '###'.

        Returns
        -------
        None

        """
        # TODO confirm there is no other subheader with the same name
        line : str = '\n' if self.previous else ''
        line += '### ' + subhdr_text + '\n'

        self.out_f.write(line)
        self.out_f.flush()

        self.previous = True

    def write_code_blk(self, code : str, lang : str=''):
        """
        Write a code block, enclosed within a prime triplet '```' pair

        Parameters
        ----------
        code : str
            A code block, eventually written in the language specified.
        lang : str, optional
            The name of a language. The default is ''.

        Returns
        -------
        None.

        """
        block : str = '\n' if self.previous else ''
        block += '```' + (' ' + lang if lang else'') + '\n'
        block += code
        block += '\n```\n'

        self.out_f.write(block)
        self.out_f.flush()

        self.previous = True

    def _write_tbl_row(self, row : List[str]) -> None:
        line = '| '
        line += '| '.join(row) + '|\n'
        self.out_f.write(line)
        self.out_f.flush()

    def _write_tbl_header(self, n_cols : int) -> None:
        line = '| '
        line += '| '.join(['--- ']*n_cols) + '|\n'

        self.out_f.write(line)
        self.out_f.flush()

    def write_table( self, tbl_rows : List[List[str]] ) -> None:
        """
        Write a text 2D array as a Markdown table

        Parameters
        ----------
        tbl_rows : List[List[str]]
            A 2D array where the 1st line is the header, and the remaining
            ones are the table rows.

        Returns
        -------
        None

        """
        if self.previous:
            self.out_f.write('\n')
            self.out_f.flush()

        self._write_tbl_row(tbl_rows[0])
        self._write_tbl_header(len(tbl_rows[0]))

        for row in tbl_rows[1: ]:
            self._write_tbl_row(row)

        self.previous = True

    def write_image(self,
                    img_name : str, alt_text : str='', desc : str='') -> None:
        """
        Include an image in the writing point

        Parameters
        ----------
        img_name : str
            Name and location of the image file.
        alt_text : str, optional
            Alternate description of the image. The default is ''.
        desc : str, optional
            Image caption. The default is ''.

        Returns
        -------
        None.

        """
        line : str = '\n' if self.previous else ''
        line += f'![{alt_text}]({img_name})'

        if desc:
            line += f'\n\n{desc}'

        self.out_f.write(line + '\n')
        self.out_f.flush()

        self.previous = True

    def close(self) -> None:
        """
        Close the Markdown file, possibly adding a newline '\n'

        Returns
        -------
        None

        """
        if self.previous:
            self.out_f.write('\n')

        self.out_f.close()

def main(argv : List[str]) -> int:
    """
    Tiny test for `GenMardown` class

    Parameters
    ----------
    argv : List[str]
        Command line parameters -- only the 1st one is used, as the output
        Markdown file.

    Returns
    -------
    int
        DESCRIPTION.

    """
    md_name : str = 'test.md'

    if len(argv) > 1:
        md_name = argv[1]

    with open(md_name, 'w', encoding='utf8') as out_f:
        mark_creator = GenMarkdown(out_f)

        mark_creator.write_header('Test header')
        mark_creator.write_subheader('Test subheader')
        mark_creator.write_subsubheader('Test subsubheader')
        mark_creator.write_code_blk('print("Hello, world !")', 'Python')

        tbl_rows = [\
            ['Col 1', 'Col 2', 'Col 3'],
            ['Txt 1', 'Txt 2', 'Txt 3']
        ]

        mark_creator.write_table(tbl_rows)

        mark_creator.write_subsubheader('Subsubheader for an image')
        mark_creator.write_image('BoaNoite.jpeg',
                                 'Good evening or good night')

        mark_creator.write_subsubheader('Subsubheader for another image')
        mark_creator.write_image('BomDia_20240125.0.jpeg',
                                 'Good morning',
                                 'Good morning')



        # mark_creator.close()

    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
