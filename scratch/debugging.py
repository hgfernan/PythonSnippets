#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Attention to detail
Created on Sun Nov 24 12:49:20 2024

@author: hilton
"""

lower : int = -1
adf : float = -3.6087663264155836
tbl = [['Lower bound', 'Value']]
critical_values = {
    '1%' : -3.9678699195021783, 
    '5%' : -3.414898316296852,
    '10%' : -3.129644744825171
}

keys = list(critical_values.keys())

print(f'adf\t{adf}')
for ind in range(len(keys)):
    key = keys[ind]
    tbl.append([key, critical_values[key]])
    
    print(key, critical_values[key], (adf < critical_values[key]))
    print(f'lower {lower}\tcritical_values[{key}]\t{critical_values[key]}')
    if (lower == -1) and (adf < critical_values[key]):
        lower = ind
        
print(f'lower {lower}\t{keys[lower]}')

print('\ntbl\n{tbl}')

if lower == -1:
    key = keys[-1]
    line = f'The series is not stationary, on the {key} confidence level'
else:
    key = keys[lower]
    line = f'The series is stationary, on the {key} confidence level'

print('\n' + line)