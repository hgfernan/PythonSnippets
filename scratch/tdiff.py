#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Experiment to create recursive differencing 
Created on Mon Nov 11 00:01:14 2024

@author: hilton
"""

import sys         # exit()
import numpy as np # class ndarray

def diff_wh(order : int, pos : int, area : np.ndarray) -> np.ndarray:
    """
    Workhorse for `diff()`: create a difference array recursively

    Parameters
    ----------
    order : int
        The order of the difference equation.
    pos : int
        .
    area : np.ndarray
        DESCRIPTION.

    Returns
    -------
    np.ndarray
        The difference array 

    """
    if (order + 1 + pos) > area.shape[0]:
        raise IndexError('Maximum index will be larger than array size')
        
    # print(f'entering: order {order}, pos {pos}, area {area}')    
    new_area : np.ndarray = area.copy()
    if order == 1:
        new_area[pos] += +1.0
        new_area[pos + 1] += -1.0
        
        return new_area
    
    new_area[pos] += 1.0
    new_area[pos + 1] += -1.0
    
    # print(f'leaving: order {order}, pos {pos}, new_area {new_area}')
    
    result : np.ndarray = \
        diff_wh(order - 1, pos, new_area) - diff_wh(order - 1, pos + 1, new_area)
    
    # print(f'result: order {order}, pos {pos}, result {result}')
        
    return result
    
def diff(order : int) -> np.ndarray:    
    result : np.array = diff_wh( order, 0, np.zeros((order + 1)) )
    
    return result

def main() -> int:
    for ind in range(1, 5):
        fmt = f'diff({ind})\t' + '{}'
        print(fmt.format(diff(ind)))
    
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main())
