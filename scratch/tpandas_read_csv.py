#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Trying to make a data file to behave
Created on Mon Feb 24 18:31:08 2025

@author: hilton
"""

# import sys     # argv, exit()
import csv     # QUOTE_NONNUMERIC
# import sqlite3 # class Connection, class Cursor, connect()

# from typing import Any, Dict, List, Tuple

import pandas as pd

df = pd.read_csv('sp500_transf.dat',
                 header=0,
                 index_col=0,
                 quoting=csv.QUOTE_NONNUMERIC,
                 parse_dates=True,
                 skiprows=4,
                 # infer_datetime_format=True,
                 # delim_whitespace=True)
                 sep='\\s+')

print(df.head())