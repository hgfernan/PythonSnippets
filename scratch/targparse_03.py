#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 12:42:43 2024

@author: hilton
"""

import sys
from argparse import _, ArgumentParser, ArgumentError, Action

class MyArgumentParser(ArgumentParser):
    def __init__(self, **kwargs): 
        super().__init__(kwargs)
        
        self.exit_on_error = True
        if 'exit_on_error' in kwargs:
            self._exit_on_error = kwargs['exit_on_error']
            
            
    def error(self, message):
        """error(message: string)

        Prints a usage message incorporating the message to stderr and
        exits.

        If you override this in a subclass, it should not return -- it
        should either exit or raise an exception.
        """
        self.print_usage(sys.stderr)
        
        if self._exit_on_error:
            self.exit(2, _('%s: error: %s\n') % (self.prog, message))
        else:
            # print(self.prog)
            # text = self.prog + ':' + message 
            self._print_message(_('%s: error: %s\n') % (self.prog['prog'], message), sys.stderr)

class UniqueStore(Action):
    def __call__(self, parser, namespace, values, option_string):
        if getattr(namespace, self.dest, self.default) is not self.default:
            parser.error(option_string + " appears several times.")
        setattr(namespace, self.dest, values)

parser = MyArgumentParser(exit_on_error=False, prog=sys.argv[0])
# # parser = argparse.ArgumentParser(exit_on_error=False)
# parser.add_argument('--integers', type=int)

# try:
#     parser.parse_args('--integers a'.split())
# except ArgumentError:
#     print('Catching an argumentError')
    
try:
    parser.add_argument('-f', '--foo', action=UniqueStore)
    args = parser.parse_args('-f a --foo b'.split())

# except argparse.ArgumentError as exc:
except Exception as exc:
    print('Catching an argumentError')
    print(f'\t{type(exc).__name__}: {str(exc)}')

print('\n' + str(args))

