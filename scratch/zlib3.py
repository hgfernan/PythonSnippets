#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 16 20:15:42 2024

@author: © 2023 Jon Glass.
"""

import zlib,sys

def deflate( data ):
	zlibbed_data = zlib.compress( data )
	#remove byte 0-1(header) and the last four(checksum)
	compressed_data = zlibbed_data[2:-4]
	return compressed_data
def inflate(compressed_data2):
	# -15 for the window buffer will make it ignore headers/footers
	zlibbed_data2 = zlib.decompress(compressed_data2, -15)
	return zlibbed_data2
##################################################
#1. Create test file in current working directory#
##################################################
str = "Zip Files Rule! " * 15
with open('data/uncompressed.txt', 'w') as make_file:
	make_file.write(str)
print('make_file.closed {make_file.closed}')
#################################
#2. Read in file we just created#
#################################
with open("data/uncompressed.txt", "r+b") as uncompressed_file:
	uncompressed_file_data = uncompressed_file.read()
print(f'uncompressed_file.closed {uncompressed_file.closed}')
##############################################################
#3. Make a file that contains JUST deflated (compressed) data#
#In a real world scenario, this could be an example of a zip #
#file that has a bad header or footer BUT the raw data is    #
#still intact.                                               #
##############################################################
# with open("data/compresseddata.bin", "wb") as compressed_file_data:
# 	compressed_file_data.write(deflate(uncompressed_file_data))
compressed_file_data = open('data/compresseddata.bin', 'wb')
compressed_file_data.write(deflate(uncompressed_file_data))
compressed_file_data.close()

print(f'compressed_file_data.closed {compressed_file_data.closed}')

################################################################
#4. Read file with just compressed data, decompress it and save#
#it back to disk                                               #
################################################################
with open("data/compresseddata.bin", "r+b") as read_compressed_file:
	file_data_to_be_uncompressed = read_compressed_file.read()
print(f'read_compressed_file.closed {read_compressed_file.closed}')

# with open("data/inflated.txt", "w") as inflated_file:
# 	inflated_file.write(inflate(file_data_to_be_uncompressed))
inflated_file = open('data/inflated.txt', 'wb')
inflated_file.write(inflate(file_data_to_be_uncompressed))
inflated_file.close()
    
print(f'inflated_file.closed {inflated_file.closed}')