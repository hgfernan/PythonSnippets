#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Playing with regular expressions for numbers in Python
Created on Wed Feb 26 19:18:47 2025

@author: hilton
"""

import re  # compile(), search(), match()
import sys # argv, exit()

from typing import List

def main(argv : List[str]) -> int:
    re_int : str = r'[\-\+]{0,1}(\d)+'
    re_fixed : str = r'[\-\+]{0,1}(\d)+\.{0,1}(\d)+'
    re_sci : str = r'[\-\+]{0,1}(\d)+\.{0,1}(\d)+[eE][\-\+]{0,1}(\d){1,4}'

    if len(argv) < 2:
        print('Usage: type a number')
    
        # Return to indicate failure
        return 1
    
    p_int   = re.compile(re_int)
    p_fixed = re.compile(re_fixed)
    p_sci   = re.compile(re_sci)
        
    for ind in range(1, len(argv)):
        print(argv[ind])
        
        rv = p_int.search(argv[ind])
        if rv is None: 
            print('No int')
        else:
            print(f'int: {rv}')
            print(dir(rv))
        
        rv = p_fixed.search(argv[ind])
        if rv is None: 
            print('No fixed point match')
        else:
            print(f'fixed: {rv}')
        
        rv = p_sci.search(argv[ind])
        if rv is None: 
            print('No scientific notation match')
        else:
            print(f'sci: {rv}')
        
        print()
        
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
