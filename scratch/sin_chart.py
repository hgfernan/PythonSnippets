#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Showing a representation of sin(x) for the interval $[0, \pi]$

Created on Sat Oct  7 19:02:22 2023

@author: user
"""

import sys                         # argv, exit()
import numpy                       # linspace(), sin()
import matplotlib.pyplot as pyplot # pyplot

from typing import List

def main(argv : List[str]) -> int:
    x = numpy.linspace(0, 2*numpy.pi, 201)
    y = numpy.sin(x)
    
    pyplot.plot(x, y)
    pyplot.xlabel('Angle [rad]')
    pyplot.ylabel('sin(x)')
    pyplot.axis('tight')
    pyplot.grid(visible=True, axis='y')
    pyplot.show()
    
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
