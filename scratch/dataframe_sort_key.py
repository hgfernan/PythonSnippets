#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
How to sort a dataframe
Created on Wed Oct  2 10:58:13 2024

@author: @wuiover from stackoverflow.com
* From:
    * https://stackoverflow.com/questions/52475458/how-to-sort-pandas-dataframe-with-a-key
    * https://saturncloud.io/blog/how-to-combine-two-columns-in-a-pandas-dataframe/ 
"""

import numpy as np
import pandas as pd

df = pd.DataFrame(['aa', 'dd', 'DD', 'AA'], columns=["data"])
df2 = pd.DataFrame()

df2['Rec'] = [1, 2, 5, 10, 20, 50]
df2['File'] = ['r', 'q', 'r', 'q', 'r', 'q']

# This is the sorting rule
rule = {
    "DD": 1,
    "AA": 10,
    "aa": 20,
    "dd": 30,
    }


def particular_sort(series):
    """
    Must return one Series
    """
    return series.apply(lambda x: rule.get(x, 1000))

def combine_columns(row) -> int:
    return row['Rec'] + (10000 if row['File'] == 'q' else 0)

new_df = df.sort_values(by=["data"], key=particular_sort)
print(new_df)  # DD, AA, aa, dd

df2['Multiple'] = df2.apply(combine_columns, axis=1)
print(df2)

new_df2 = df2.sort_values(by='Multiple').drop('Multiple', axis=1)

print(new_df)  # DD, AA, aa, dd
print(new_df2) # [1 'r', 5 'r', 20 'r', ]
