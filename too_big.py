from math import factorial
import time

NMAX=200
somme=0
chrono=time.time()
for i in range(1,NMAX+1):
	somme+=factorial(i)

print("1! + 2! + ... +",str(NMAX)+"!=",somme)
print("Temps d'exécution :",time.time()-chrono,"sec.")
