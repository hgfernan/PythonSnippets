'''
Created on Oct 25, 2017

@author: hilton
'''

global aGlobal
aGlobal = 0

def aFunction ():
    global aGlobal
    
    print ('aFunction() was called')
    aGlobal += 1

print ('__init__ file was called')
print ('__name__ == {0}'.format (__name__))
print ('__package__ == {0}'.format (__package__))
print ('')

print ( "dir () == {0}".format (dir ()) )
print ( "dir (__builtins__) == {0}".format (dir (__builtins__)) )
print ( "dir (__file__) == {0}".format (dir (__file__)) )
print ( "dir (__package__) == {0}".format (dir (__package__)) )

if __name__ == __package__:
    print ('Was invoked as {0}'.format (__package__))
