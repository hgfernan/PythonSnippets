'''
Created on Oct 27, 2017

@author: hilton

Slightly modified 
From:
* http://www.diveintopython3.net/http-web-services.html
* http://httplib2.readthedocs.io/en/latest/libhttplib2.html#examples
'''

import httplib2

print ( 'dir (httplib2): {0}\n'.format (dir (httplib2)) )

h = httplib2.Http ('.cache')
response, content = \
    h.request ('http://diveintopython3.org/examples/feed.xml')

print ('response: {0}\n\ncontent: {1}'.format (response, content))

print ('response.status: {0}\n'.format (response.status))

# From:
# http://httplib2.readthedocs.io/en/latest/libhttplib2.html#examples

print ( "dir (h): {0}\n\n".format (dir (h)) )

# import httplib2
h = httplib2.Http()
resp, content = h.request("http://bitworking.org/")
assert resp.status == 200

print (resp['content-type'])
# assert resp['content-type'] == 'text/html'
# UPDATED to 
assert resp['content-type'] == 'text/html; charset=utf-8'


conn = httplib2.Http ('.cache')
response, content = \
    conn.request ('https://cex.io/api/ticker/BTC/USD')
 
print ('response: {0}\n\ncontent: {1}'.format (response, content))
 
print ('response.status: {0}\n'.format (response.status))

