#! /usr/bin/env python3 

import sys # argv, exit() 

def fl_print(*value, sep=' ', end='\n', file=sys.stdout): 
	result = print(*value, sep=sep, end=end, file=file, flush=True)
	print(f'type(value) {type(value)}')

	# Normal function termination
	return result

def main(args = list[str]) -> int:
	print('Printing with print()')
	print('a')

	print(f'a {10}')
	print('a', 10)
	print('a', 10, 3.141596)
	print('a', 10, 3.141596, False)

	print('\nPrinting with fl_print()')
	fl_print('a')

	fl_print(f'a {10}')
	fl_print('a', 10)
	fl_print('a', 10, 3.141596)
	fl_print('a', 10, 3.141596, False)

	# Normal function termination
	return 0

if __name__ == '__main__':
	sys.exit(main(sys.argv))

