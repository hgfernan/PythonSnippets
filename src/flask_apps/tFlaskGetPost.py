#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 18 12:44:05 2023

@author: vegibit
From: 
* https://vegibit.com/how-to-use-forms-in-python-flask/
Previously:
app.py
"""

import sys  # argv, exit()
import json # dumps() 

from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/shortenurl', methods=['GET', 'POST'])
def shortenurl():
    print(f'shortenurl: request.method\n{request.method}')
    if request.method == 'POST':
        print(f'type(request)\n{type(request)}')
        print(f'dir(request)\n{dir(request)}')
        print(f'request\n{request}')
        form = request.form
        print(f'request.form\n{form}')
        print(f'json.dumps(form)\n{json.dumps(form, indent=2)}')
        
        return render_template('shortenurl.html', 
                               shortcode=request.form['shortcode'])
    elif request.method == 'GET':
        return render_template('home.html')
    else:
      
        return 'Not a valid request method for this route'
    
def main(argv : list[str]) -> int:
    app.run(debug=True)
    
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
