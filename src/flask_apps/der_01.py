#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Tiny little Flask code 

Created on Tue Jan 10 18:25:35 2023

@author: Daniel Ellis Research
From:
* https://towardsdatascience.com/talking-to-python-from-javascript-flask-and-the-fetch-api-e0ef3573c451
"""

########  imports  ##########
from flask import Flask, jsonify, request, render_template
app = Flask(__name__)
#############################
# Additional code goes here #
#############################

@app.route('/index_der-01')
def home_page():
    example_embed='This string is from python'
    return render_template('index_der-01.html', embed=example_embed)

#########  run app  #########
app.run(debug=True)