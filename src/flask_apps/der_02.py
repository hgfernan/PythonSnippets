#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Just another tiny little Flask code 

Created on Tue Jan 10 21:18:25 2023

@author: Daniel Ellis Research
From:
* https://towardsdatascience.com/talking-to-python-from-javascript-flask-and-the-fetch-api-e0ef3573c451
"""

########  imports  ##########
from flask import Flask, jsonify, request, render_template
app = Flask(__name__)
#############################
# Additional code goes here #
#############################

@app.route('/index_der-02')
def home_page():
    example_embed='This string is from python'
    return render_template('index_der-01.html', embed=example_embed)

@app.route('/test_der-02', methods=['GET', 'POST'])
def testfn():    # GET request
    if request.method == 'GET':
        message = {'greeting':'Hello from Flask!'}
        return jsonify(message)  # serialize and use JSON headers    # POST request
    
    if request.method == 'POST':
        print(request.get_json())  # parse as JSON
        return 'Sucesss', 200
    
#########  run app  #########
app.run(debug=True)