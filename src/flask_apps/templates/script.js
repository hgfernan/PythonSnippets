/*
Tiny script to xfer information to a text page, using JavaScript `fetch()`

Created on Fri Jan  6 10:00:46 2023

@author: StackOverflow's @TheSmartMonkey and contributors
From:
https://stackoverflow.com/questions/57891275/simple-fetch-get-request-in-javascript-to-a-flask-server
*/

function getHello() {
    const url = 'http://localhost:8989/hello'
    const response = fetch(url)
    console.log(response);
    document.getElementById("demo").innerHTML = response;
}
