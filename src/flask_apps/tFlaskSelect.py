#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Showing an image in a Flask app

Created on Thu Jan  5 18:30:21 2023

@author: hilton
From:
* https://stackoverflow.com/questions/46785507/python-flask-display-image-on-a-html-page
"""
# from flask import Flask, render_template, request
from flask import Flask, render_template

import os

img_dir = os.path.join('static', 'img')

app = Flask(__name__)
app.config['IMG_DIR'] = img_dir

@app.route('/', methods=['GET'])
def dropdown():
    colours = ['Binance', 'Kraken']
    return render_template('test.html', colours=colours)

if __name__ == '__main__':
    app.run(debug=True)
