#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Testbed of the use of JavaScript fetch() to activate Python Flask route

Created on Mon Jan  9 16:10:26 2023

@author: hilton
"""

import sys # argv, exit()
# import urllib.parse # urlencode()

from flask import Flask, render_template, request

# Initialize the Flask application
app = Flask(__name__)

g_count = 0

@app.route('/comm/<int:count>', methods=['GET', 'POST'])
def comm_count(count : int):
    global g_count
    print(dir(request))
    print(request.method)
    
    if request.method == 'GET':
        g_count +=1 
        return render_template('index_comm.html', count=g_count)    
    else: 
        return "POST"

@app.route('/comm/', methods=['GET', 'POST'])
def comm():
    global g_count
    print(dir(request))
    print(request.method)
    
    if request.method == 'GET':
        g_count +=1 
        return render_template('index_comm.html', count=g_count)    
    else: 
        return "POST"


def main(argv : list[str]) -> int:
    app.run(debug=True)
    
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))


