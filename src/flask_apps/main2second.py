#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Use of one of two pages, according to the HtML method

Created on Mon Jan  9 09:09:54 2023

@author: hilton
"""

import sys # argv, exit()

from flask import Flask, render_template, request

# Initialize the Flask application
app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('main_form.html')
    
    elif request.method == 'POST':
        return render_template('vertical_tabs.html')


def main(argv : list[str]) -> int:
    app.run(debug=True)
    
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
