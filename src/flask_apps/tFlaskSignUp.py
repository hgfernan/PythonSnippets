#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  6 12:29:43 2023

@author: geeks4geeks
From:
https://www.geeksforgeeks.org/retrieving-html-from-data-using-flask/
"""

# importing Flask and other modules
from flask import Flask, request, render_template
 
# Flask constructor
app = Flask(__name__)  
 
# A decorator used to tell the application
# which URL is associated function
@app.route('/simple_signup', methods =["GET", "POST"])
def gfg():
    if request.method == 'POST':
       # getting input with name = fname in HTML form
       user_name = request.form.get('uname')
       # getting input with name = lname in HTML form
       mail = request.form.get('mail')
       
       msg = 'Hello, ' + user_name + mail
       print(f'type(msg) \'{type(msg)}\'')
       print(f'msg \'{msg}\'')
       
       # Normal function termination
       return msg
   
    return render_template("simple_signup.html")
 
if __name__=='__main__':
   app.run(debug=True)

