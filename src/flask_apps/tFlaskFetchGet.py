#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sample code on the use of `fetch()` in a text page, using GET
Created on Fri Jan  6 09:55:49 2023

@author: StackOverflow's @TheSmartMonkey and contributors
From:
https://stackoverflow.com/questions/57891275/simple-fetch-get-request-in-javascript-to-a-flask-server

OBS: file originally called `server.py`
"""

import json # dumps()

# from flask import Flask, request, jsonify
from flask import Flask, jsonify

app = Flask(__name__)


@app.route('/hello', methods=['GET'])
def hello():
    jsonResp = {'jack': 4098, 'sape': 4139, 'skrbndanski' : 0}
    print(f'jsonResp \'{json.dumps(jsonResp, indent=2)}\'')
    
    msg = jsonify(jsonResp)
    print(f'type(msg) \'{type(msg)}\'')
    print(f'msg \'{msg}\'')
    return msg

if __name__ == '__main__':
    app.run(host='localhost', port=8989)

