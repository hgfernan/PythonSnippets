#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Upload page 

Created on Mon Jan  9 08:09:10 2023

@author: hilton
"""

import sys # argv, exit()

from flask import Flask, render_template

# Initialize the Flask application
app = Flask(__name__)

@app.route('/')
def index():

    """
    initialize drop down menus
    """

    return render_template('vertical_tabs.html')


def main(argv : list[str]) -> int:
    app.run(debug=True)
    
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
