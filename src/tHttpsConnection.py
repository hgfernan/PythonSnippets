'''
Created on Oct 27, 2017

@author: hilton
From: 

Status: FAILED for cex.io -- Status 301

BUG: PyDev insists that HTTPSConnection is an undefined variable, 
     but Python3 uses it normally 
'''

import http.client

conn = http.client.HTTPSConnection ("www.cex.io")
header = ['User-Agent', 'Mozilla 5.10']

# conn.putrequest ("GET", '/api/ticker/BTC/USD')
# conn.putheader (header[0], header[1])
# conn.endheaders ()
# conn.send ('\n'.encode (encoding='utf_8'))

conn.request ("GET", '/api/ticker/BTC/USD')

print ("\ndir (conn): {0}\n".format (dir (conn)) )

print ( "conn: {0}\n".format (str (conn)) )

r1 = conn.getresponse ()

print ("\ndir (r1):{0}\n".format (dir (r1)) )

print (r1.info ())
print (r1.getheaders ())
print (r1.status, r1.reason)
print (str (r1)) 
# 200 OK

data = r1.read ()
print (data)
