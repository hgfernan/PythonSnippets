#! /usr/bin/env python3 

## 
 # A script from Python documentation that starts a shell, and saves everything typed in 
 # it in a file.
 # 
 # Slightly changed 
 # From<br/>
 # <ul>
 #      <li>
 #          <a href="https://docs.python.org/3.5/library/pty.html">
 #              35.8. pty — Pseudo-terminal utilities
 #          </a>
 #      </li>
 # </ul>
 # 

import os
import pty
import sys
import time
import argparse

parser = argparse.ArgumentParser ()
parser.add_argument ('-a', '--append', 
                     dest = 'append', action = 'store_true',
                     help = 'Append current section to a previous recording'
                    )
parser.add_argument ('-p', '--python',
                     dest='use_python', action='store_true',
                     help = 'Start a session with the Python interpreter shell.'
                    )
parser.add_argument ('-e', '--execute', metavar = 'EXEC',
                     dest = 'execute',
                     help = 'Name of an executable file name.'
                    )
parser.add_argument ('filename', nargs='?', default='typescript')

options = parser.parse_args ()

fExecute = (options.execute == None)
fPython  = options.use_python

if (fExecute ^ fPython): 
    print ("An executable should be chosen")
    
    sys.exit (1)
    
if options.execute != None:
    executable = options.execute 
    
elif options.use_python: 
    executable = sys.executable
    
filename = options.filename
mode = 'ab' if options.append else 'wb'

with open (filename, mode) as script:
    def read(fd):
        data = os.read (fd, 1024) 
        script.write (data)
        script.flush ()
        return data

    print ('Script started, file is', filename)
    script.write (('Script started on %s\n' % time.asctime ()).encode ())

    pty.spawn (executable, read)

    script.write (('Script done on %s\n' % time.asctime()).encode())
    print ('Script done, file is', filename)

script.close ()