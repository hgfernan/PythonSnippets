#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sample code to and experiments with standard module `multiprocessing`

Created on Wed Jan  4 13:32:20 2023

@author: hilton
From: 
* https://docs.python.org/3.9/library/multiprocessing.html
* https://github.com/pytorch/pytorch/issues/3492#issuecomment-522393847
"""

import os  # getpid(), getppid()
import sys # argv, exit()


import multiprocessing # class Process, set_start_method()

def child(name : str, q : multiprocessing.Queue) -> None:
    print('child: Hello,', name, '!')
    print(f'child: I am {os.getpid()}, and my parent is {os.getppid()}')
    
    # OBS even non blocking calls need a non-full queue
    
    print(f'child: q.empty() {q.empty()}')
    if not q.empty():
        rv = q.get(False, 0)
        print(f'child: From get_nowait() rv `{rv}`')
    
    rv = q.put('Aaaaa')
    print(f'child: From put() rv `{rv}`')
    
    # Normal function termination
    return

def main(argv):
    ctx = multiprocessing.get_context("spawn")
    q = ctx.Queue()
    
    print(f'main: I am {os.getpid()}, and my parent is {os.getppid()}')
    p = ctx.Process(target=child, args=('Child process', q,))
    # p = multiprocessing.Process(target=child, args=('Child process', q,))
    p.start()
    
    msg = q.get()
    print(f'main: msg `{msg}`')
    
    p.join()
    
    # Normal function termination
    return 0
    
if __name__ == '__main__':
    sys.exit(main(sys.argv))
