#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 17 17:52:44 2023

@author: ZetCode
From: 
https://zetcode.com/python/bcrypt/

Originally
check_passwd.py
"""

import bcrypt

passwd = b's$cret12'

salt   = bcrypt.gensalt()
hashed = bcrypt.hashpw(passwd, salt)

if bcrypt.checkpw(passwd, hashed):
    print("match")
else:
    print("does not match")