#! /usr/bin/env python3 

# 
# Slightly adapted</br>
# From<br/>
# <ol>
#	<li>
#		<a href="https://docs.python.org/3.5/howto/regex.html">
#			Regular Expression HOWTO
#		</a>
#		Author:	A.M. Kuchling <amk@amk.ca>
#	<\li>
# /<ol>


import re  # match(), end()
import sys # __stdin__

inp_f = sys.__stdin__
out_f = sys.__stdout__

# Will match a file with lines like 
# 1 - I'd rather be forest than a tree
# 2 - Yes, I would.
# 3 - If I could 
# 4 - That surely would

p = re.compile ('[0-9]+ - ')

# line = inp_f.readline ().decode ('utf-8')
line = inp_f.readline ()

while (line != ''):
    m = p.match (line)
    if m:
        remain = line[m.end () : ]
        out_f.write  (remain)

#    line = inp_f.readline ().decode ('utf-8')
    line = inp_f.readline ()

inp_f.close ()
out_f.close ()


