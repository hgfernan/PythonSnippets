#! /usr/bin/env python3

import sympy

a = sympy.Symbol ('a')

rA = sympy.Symbol ('rA')
rB = sympy.Symbol ('rB')

wS = sympy.Symbol ('wS')
wT = sympy.Symbol ('wT')

wS = a
wT = a

data = {}
data['a'] = a
data['rA'] = rA
data['rB'] = rB
data['wS'] = wS
data['wT'] = wT

myKeys = ['a', 'rA', 'rB', 'wS', 'wT']
for k in myKeys: 
    print ( '{0} == {1}'.format (k, data[k]) )


print ('\nFirst sell')

print ("\tBefore assignment: wT {0}, data['wT'] {1}".
           format (wT, data['wT']))
wS = sympy.factor (wS - rA * a / rB)
wT = sympy.factor (wT + rB * a / rA)


print ( '{0} == {1}'.format ('wS', wS) )
print ( '{0} == {1}'.format ('wT', wT) )
print ( '{0} == {1}'.format ('sympy.simplify (wS)', sympy.simplify (wS)) )

print ("\tAfter assignment: wT {0}, data['wT'] {1}".
           format (wT, data['wT']))

resWS = wS.subs (a, 0.5)
print ("resWS = wS.subs (a, 0.5) == {0}".format (resWS))
resWS = resWS.subs (rB, 7000.0)
print ("resWS.subs (rB, 7000.0) == {0}".format (resWS))
resWS = resWS.subs (rA, 7005.0)
print ("resWS.subs (rB, 7005.0) == {0}".format (resWS))

resWT = wT.subs (a, 0.5)
print ("resWT = wT.subs (a, 0.5) == {0}".format (resWT))
resWT = resWT.subs (rB, 7000.0)
print ("resWT.subs (rB, 7000.0) == {0}".format (resWT))
resWT = resWT.subs (rA, 7005.0)
print ("resWT.subs (rA, 7005.0) == {0}".format (resWT))
