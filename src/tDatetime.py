#! /usr/bin/env python3 

'''
Created on Jul 3, 2019

@author: hilton
'''

import sys # argv, exit()
import datetime # Class date

def main(argv):
    if len(argv) < 3:
        print( "{0}: please inform date and increment".format(argv[0]) )
        return 1
        
    try:
        dt = datetime.datetime.strptime(argv[1], "%Y-%m-%d")
        d = datetime.date(dt.year, dt.month, dt.day)
#        print(d)
        i = int(argv[2]) 
        
    except Exception as e:
        print(type(e))
        print(e.args)
        print(e)
        
        return 2
    
    
    out = d + datetime.timedelta(days = i)
    print(out)
    
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
    