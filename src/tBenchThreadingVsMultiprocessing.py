#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 12 16:31:21 2023

@author: Melvin Koh @melvinkcx2
From:
* https://hackernoon.com/concurrent-programming-in-python-is-not-what-you-think-it-is-b6439c3f3e6a

"""
import sys             # argv, exit()
import threading       # class Thread
import multiprocessing # class Process


def countdown():
    x = 1000000000
    while x > 0:
        x -= 1

    # Normal function termination
    return


# Implementation 1: Multi-threading
def implementation_1():
    print('Threads')
    
    thread_1 = threading.Thread(target=countdown)
    thread_2 = threading.Thread(target=countdown)
    thread_1.start()
    thread_2.start()
    thread_1.join()
    thread_2.join()

    # Normal function termination
    return


# Implementation 2: Run in serial
def implementation_2():
    print('Sequential')
    
    countdown()
    countdown()

    # Normal function termination
    return


def implementation_3():
    print('Processes')
    
    process_1 = multiprocessing.Process(target=countdown)
    process_2 = multiprocessing.Process(target=countdown)
    process_1.start()
    process_2.start()
    process_1.join()
    process_2.join()

    # Normal function termination
    return


def usage():
    print('Usage')
    print('./tBenchThreadingVsMultiprocessing.py {1|2|3}`')

    # Normal function termination
    return
    
    
def main(argv : list[str]) -> int:
    if len(argv) < 2:
        usage()
        
        # Return to indicate failure
        return 1 
        
    if argv[1] == '1':
        implementation_1()

    elif argv[1] == '2':
        implementation_2()

    elif argv[1] == '3':
        implementation_3()
        
    else:
        usage()
        
        # Return to indicate failure
        return 1 
        
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
        
