#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 17 17:46:42 2023

@author: ZetCode
From: 
https://zetcode.com/python/bcrypt/

Originally
create_hashed_password.py
"""

import bcrypt

passwd = b's$cret12'

salt = bcrypt.gensalt()
hashed = bcrypt.hashpw(passwd, salt)

print(f'salt   {salt}')
print(f'hashed {hashed}')