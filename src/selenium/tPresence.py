#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Testbed for explicit Selenium wait

Created on Tue Oct  4 11:27:54 2022

@author: Authors of the documentation of Selenium Python binding
* https://selenium-python.readthedocs.io/waits.html
"""

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver = webdriver.Chrome()
driver.get('https://twitter.com/')
try:
    # element = WebDriverWait(driver, 20).until(
    #     EC.presence_of_element_located((By.CLASS_NAME, 'css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0'))
    # )
    element = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, '[data-testid=loginButton]'))
    )
    print(f'element\n{element}')
    print(f'dir(element)\n{dir(element)}')
    print(f'element.accessible_name\n{element.accessible_name}')
    print(f'element.id\n{element.id}')
    print(f'element.tag_name\n{element.tag_name}')
    
except Exception as exc:
    print(type(exc).__name__ + ': ' + str(exc))
    
finally:
    print('DONE')
    driver.quit()
    
# class=""    