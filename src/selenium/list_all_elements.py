#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 29 19:56:26 2022

@author: Selenium documentation crew
From:
* https://selenium-python.readthedocs.io/getting-started.html
"""

import sys # exit()
import time # sleep()
from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

# driver = webdriver.Firefox()
driver = webdriver.Chrome()
# driver.get("http://www.python.org")
driver.get("https://twitter.com/")
driver.implicitly_wait(10) # seconds

print(f'driver.title {driver.title}')

assert "Twitter" in driver.title
# elem = driver.find_element(By.NAME, "q")
# elem = driver.find_element(By.CLASS_NAME, "css-4rbku5")
# elem = driver.find_element(By.CSS_SELECTOR, '[data-testid=loginButton]')
# elems = driver.find_elements(By.CSS_SELECTOR, '[role=link]')
# elems = driver.find_elements(By.NAME, "*")
elems = driver.find_elements(By.CSS_SELECTOR, '*')

print(f'elems\n{elems}')
print(f'len(elems) {len(elems)}')
print(f'type(elems) {type(elems)}')

time.sleep(10)
for ind in range(len(elems)):
    try:
        line = f'elems[{ind:2d}].tag_name \'{elems[ind].tag_name}\' ' 
        line += f".get_attribute('class') '{elems[ind].get_attribute('class')}'"
        print(line)
    except Exception as exc:
        print(type(exc).__name__ + ': ' + str(exc))
        # print(type(exc).__name__ + ':')
    
if len(elems) > 0:
    elem = elems[-1]
    print(elem)
    
    print(dir(elem))
    print(f'elem.text                  \'{elem.text}\'')
    print(f'elem.tag_name              \'{elem.tag_name}\'')
    # print(f'elem.get_attribute()       \'{elem.get_attribute()}\'')
    print(f'elem.accessible_name       \'{elem.accessible_name}\'')
    print(f'elem.submit                \'{elem.submit}\'')
    print(f'elem.value_of_css_property \'{elem.value_of_css_property}\'')

sys.exit()

# rv = elem.clear()
# print(f'elem.clear() {rv}')

# rv = elem.send_keys("pycon")
# print(f'elem.send_keys() {rv}')

# rv = elem.send_keys(Keys.RETURN)
# print(f'elem.send_keys() {rv}')

actions = ActionChains(driver)
actions.move_to_element(elem)
actions.click(elem)
actions.perform()

#layers > div:nth-child(2) > div > div > div > div > div > div.css-1dbjc4n.r-1awozwy.r-18u37iz.r-1pi2tsx.r-1777fci.r-1xcajam.r-ipm5af.r-g6jmlv > div.css-1dbjc4n.r-1867qdf.r-1wbh5a2.r-kwpbio.r-rsyp9y.r-1pjcn9w.r-1279nm1.r-htvplk.r-1udh08x > div > div > div.css-1dbjc4n.r-14lw9ot.r-6koalj.r-16y2uox.r-1wbh5a2 > div.css-1dbjc4n.r-16y2uox.r-1wbh5a2.r-1jgb5lz.r-1ye8kvj.r-13qz1uu > div > div > div > div.css-1dbjc4n.r-mk0yit.r-1f1sjgu.r-13qz1uu > label > div > div.css-1dbjc4n.r-18u37iz.r-16y2uox.r-1wbh5a2.r-1wzrnnt.r-1udh08x.r-xd6kpl.r-1pn2ns4.r-ttdzmv > div > input

# xpath = '//*[@id="layers"]/div[2]/div/div/div/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div/div/div/div[5]/label/div/div[2]/div/input'
# xpath = '/html/body/div/div/div/div[1]/div[2]/div/div/div/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div/div/div/div[5]/label/div/div[2]/div/input'

# elem = driver.find_element(By.XPATH, xpath)
elem = driver.find_element(By.CSS_SELECTOR, '[role=dialog]')

print(f'elem.tag_name              \'{elem.tag_name}\'')
print(f'elem.text                  \'{elem.text}\'')
print(f'elem.value_of_css_property\n\'{elem.value_of_css_property}\'')
print(f'dir(elem) {dir(elem)}')

elem2 = elem.find_element(By.ID, "div")
print(f'elem2.tag_name              \'{elem2.tag_name}\'')
print(f'elem2.text                  \'{elem2.text}\'')
print(f'elem2.value_of_css_property\n\'{elem2.value_of_css_property}\'')
print(f'dir(elem) {dir(elem)}')

time.sleep(15)

# rv = driver.page_source
# print(f'driver.page_source\n{rv}')

assert "No results found." not in driver.page_source
driver.close()

# class="css-4rbku5 css-18t94o4 css-1dbjc4n r-1niwhzg r-1ets6dv r-sdzlij r-1phboty r-rs99b7 r-1loqt21 r-a9p05 r-eu3ka r-5oul0u r-17w48nw r-2yi16 r-1qi8awa r-1ny4l3l r-ymttw5 r-o7ynqc r-6416eg r-lrvibr r-1ipicw7"