#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 29 19:56:26 2022

@author: Selenium documentation crew
From:
* https://selenium-python.readthedocs.io/getting-started.html
* https://selenium-python.readthedocs.io/waits.html
"""

import sys  # exit()
import time # sleep()

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select

# driver = webdriver.Firefox()
driver = webdriver.Chrome()
# driver.get("http://www.python.org")
driver.get("https://twitter.com/")
print(f'driver.title {driver.title}')

assert "Twitter" in driver.title

elem = None
try:
    # global elem
    # element = WebDriverWait(driver, 20).until(
    #     EC.presence_of_element_located((By.CLASS_NAME, 'css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0'))
    # )
    element_in = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, '[data-testid=loginButton]'))
    )
    elem = element_in
    print(f'elem\n{elem}')
    print(f'dir(elem)\n{dir(elem)}')
    print(f'elem.accessible_name\n{elem.accessible_name}')
    print(f'elem.id\n{elem.id}')
    print(f'elem.tag_name\n{elem.tag_name}')
    
except Exception as exc:
    print(type(exc).__name__ + ': ' + str(exc))
    
finally:
    print('Found \'loginButton\'')

print(f'elem       {elem}')
print(f'type(elem) {type(elem)}')
print(f'dir(elem)\n{dir(elem)}')

# rv = elem.clear()
# print(f'elem.clear() {rv}')

# rv = elem.send_keys("pycon")
# print(f'elem.send_keys() {rv}')

# rv = elem.send_keys(Keys.RETURN)
# print(f'elem.send_keys() {rv}')

actions = ActionChains(driver)
actions.move_to_element(elem)
actions.click(elem)
actions.perform()

time.sleep(5)
failed = False
try:
    elem2 = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, '[autocapitalize=sentences]'))
    )
    print(f'elem2\n{elem2}')
    print(f'dir(elem2)\n{dir(elem2)}')
    print(f'elem2.accessible_name\n\'{elem2.accessible_name}\'')
    print(f'elem2.id\n\'{elem2.id}\'')
    print(f'elem2.tag_name\n\'{elem2.tag_name}\'')
    
except Exception as exc:
    print(type(exc).__name__ + ': ' + str(exc))
    failed = True
    
finally:
    print('DONE getting the login field')
    # driver.quit()

if failed: 
    driver.quit()        
    
    sys.exit(1)
    
elem2.clear()
elem2.send_keys("hgfernan")
elem2.send_keys(Keys.RETURN)

# time.sleep(15)

failed = False
try:
    elem3 = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, '[type="password"]'))
    )
    print(f'elem3\n{elem3}')
    print(f'dir(elem3)\n{dir(elem3)}')
    print(f'elem3.accessible_name\n\'{elem3.accessible_name}\'')
    print(f'elem3.id\n\'{elem3.id}\'')
    print(f'elem3.tag_name\n\'{elem3.tag_name}\'')
    
except Exception as exc:
    print(type(exc).__name__ + ': ' + str(exc))
    failed = True
    
finally:
    print('DONE getting the password field')
    # driver.quit()

if failed: 
    driver.quit()        
    
    sys.exit(1)

elem3.clear()
# elem3.send_keys("")
elem3.send_keys(Keys.RETURN)

# data-testid="SideNav_NewTweet_Button"

failed = False
try:
    elem4 = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, '[data-testid=SideNav_NewTweet_Button]'))
    )
    print(f'elem4\n{elem4}')
    print(f'dir(elem4)\n{dir(elem4)}')
    print(f'elem4.accessible_name\n\'{elem4.accessible_name}\'')
    print(f'elem4.id\n\'{elem4.id}\'')
    print(f'elem4.tag_name\n\'{elem4.tag_name}\'')
    
except Exception as exc:
    print(type(exc).__name__ + ': ' + str(exc))
    failed = True
    
finally:
    print('DONE getting Tweet button')
    # driver.quit()

if failed: 
    driver.quit()        
    
    sys.exit(1)

actions.move_to_element(elem4)
actions.click(elem4)
actions.perform()

# class="notranslate public-DraftEditor-content"
# class="public-DraftStyleDefault-block public-DraftStyleDefault-ltr"
# aria-label="Tweet text"
failed = False
try:
    elem5 = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, '[aria-label="Tweet text"]'))
    )
    print(f'elem5\n{elem5}')
    print(f'dir(elem5)\n{dir(elem5)}')
    print(f'elem5.accessible_name\n\'{elem5.accessible_name}\'')
    print(f'elem5.id\n\'{elem5.id}\'')
    print(f'elem5.tag_name\n\'{elem5.tag_name}\'')
    
except Exception as exc:
    print(type(exc).__name__ + ': ' + str(exc))
    failed = True
    
finally:
    if failed:
        print('FAILED getting Tweet text')        
    else:
        print('DONE getting Tweet text')
    # driver.quit()

if failed: 
    # driver.quit()        
    
    sys.exit(1)

elem5.clear()
elem5.send_keys(' Testing tweet')
# elem5.send_keys(Keys.RETURN)

# # data-testid="tweetButton"
# failed = False
# try:
#     elem6 = WebDriverWait(driver, 20).until(
#         EC.presence_of_element_located((By.CSS_SELECTOR, '[data-testid="tweetButton"]'))
#     )
#     print(f'elem6\n{elem6}')
#     print(f'dir(elem6)\n{dir(elem6)}')
#     print(f'elem6.accessible_name\n\'{elem6.accessible_name}\'')
#     print(f'elem6.id\n\'{elem6.id}\'')
#     print(f'elem6.tag_name\n\'{elem6.tag_name}\'')
    
# except Exception as exc:
#     print(type(exc).__name__ + ': ' + str(exc))
#     failed = True
    
# finally:
#     if failed:
#         print('FAILED getting Tweet button')        
#     else:
#         print('DONE getting Tweet button')
#     # driver.quit()

# if failed: 
#     # driver.quit()        
    
#     sys.exit(1)

# actions.move_to_element(elem6)
# actions.click(elem6)
# actions.perform()

# aria-label="Schedule Tweet"
failed = False
try:
    elem7 = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, '[aria-label="Schedule Tweet"]'))
    )
    print(f'elem7\n{elem7}')
    print(f'dir(elem7)\n{dir(elem7)}')
    print(f'elem7.accessible_name\n\'{elem7.accessible_name}\'')
    print(f'elem7.id\n\'{elem7.id}\'')
    print(f'elem7.tag_name\n\'{elem7.tag_name}\'')
    
except Exception as exc:
    print(type(exc).__name__ + ': ' + str(exc))
    failed = True
    
finally:
    if failed:
        print('FAILED getting Schedule button')        
    else:
        print('DONE getting Schedule button')
    # driver.quit()

if failed: 
    # driver.quit()        
    
    sys.exit(1)

actions.move_to_element(elem7)
actions.click(elem7)
actions.perform()

# id="SELECTOR_1"
failed = False
try:
    elem75 = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.ID, 'SELECTOR_1'))
    )
    print(f'elem75\n{elem75}')
    print(f'dir(elem75)\n{dir(elem75)}')
    print(f'elem75.accessible_name\n\'{elem75.accessible_name}\'')
    print(f'elem75.id\n\'{elem75.id}\'')
    print(f'elem75.tag_name\n\'{elem75.tag_name}\'')
    
except Exception as exc:
    print(type(exc).__name__ + ': ' + str(exc))
    failed = True
    
finally:
    if failed:
        print('FAILED getting SELECTOR_1')        
    else:
        print('DONE getting SELECTOR_1')
    # driver.quit()

if failed: 
    # driver.quit()        
    
    sys.exit(1)

print('Will cast elem75 to Select')
sel = Select(elem75)
sel.select_by_value('12')

print('Selected december')

# id="SELECTOR_2"
failed = False
try:
    elem775 = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.ID, 'SELECTOR_2'))
    )
    print(f'elem775\n{elem775}')
    print(f'dir(elem775)\n{dir(elem775)}')
    print(f'elem775.accessible_name\n\'{elem775.accessible_name}\'')
    print(f'elem775.id\n\'{elem775.id}\'')
    print(f'elem775.tag_name\n\'{elem775.tag_name}\'')
    
except Exception as exc:
    print(type(exc).__name__ + ': ' + str(exc))
    failed = True
    
finally:
    if failed:
        print('FAILED getting SELECTOR_2')        
    else:
        print('DONE getting SELECTOR_2')
    # driver.quit()

if failed: 
    # driver.quit()        
    
    sys.exit(1)

print('Will cast elem775 to Select')
sel = Select(elem775)
sel.select_by_value('5')

print('Selected 12/5')

# id="SELECTOR_3"
failed = False
try:
    elem7875 = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.ID, 'SELECTOR_3'))
    )
    print(f'elem7875\n{elem7875}')
    print(f'dir(elem7875)\n{dir(elem7875)}')
    print(f'elem7875.accessible_name\n\'{elem7875.accessible_name}\'')
    print(f'elem7875.id\n\'{elem7875.id}\'')
    print(f'elem7875.tag_name\n\'{elem7875.tag_name}\'')
    
except Exception as exc:
    print(type(exc).__name__ + ': ' + str(exc))
    failed = True
    
finally:
    if failed:
        print('FAILED getting SELECTOR_3')        
    else:
        print('DONE getting SELECTOR_3')
    # driver.quit()

if failed: 
    # driver.quit()        
    
    sys.exit(1)

print('Will cast elem7875 to Select')
sel = Select(elem7875)
sel.select_by_value('2023')

print('Selected 12/5/2023')

# id="SELECTOR_4"
failed = False
try:
    elem79375 = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.ID, 'SELECTOR_4'))
    )
    print(f'elem79375\n{elem79375}')
    print(f'dir(elem79375)\n{dir(elem79375)}')
    print(f'elem79375.accessible_name\n\'{elem79375.accessible_name}\'')
    print(f'elem79375.id\n\'{elem79375.id}\'')
    print(f'elem79375.tag_name\n\'{elem79375.tag_name}\'')
    
except Exception as exc:
    print(type(exc).__name__ + ': ' + str(exc))
    failed = True
    
finally:
    if failed:
        print('FAILED getting SELECTOR_4')        
    else:
        print('DONE getting SELECTOR_4')
    # driver.quit()

if failed: 
    # driver.quit()        
    
    sys.exit(1)

print('Will cast elem79375 to Select')
sel = Select(elem79375)
sel.select_by_value('9')

print('Selected 12/5/2023 9:')

# id="SELECTOR_5" value=40
failed = False
try:
    elem796875 = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.ID, 'SELECTOR_5'))
    )
    print(f'elem79375\n{elem79375}')
    print(f'dir(elem79375)\n{dir(elem79375)}')
    print(f'elem79375.accessible_name\n\'{elem79375.accessible_name}\'')
    print(f'elem79375.id\n\'{elem79375.id}\'')
    print(f'elem79375.tag_name\n\'{elem79375.tag_name}\'')
    
except Exception as exc:
    print(type(exc).__name__ + ': ' + str(exc))
    failed = True
    
finally:
    if failed:
        print('FAILED getting SELECTOR_5')        
    else:
        print('DONE getting SELECTOR_5')
    # driver.quit()

if failed: 
    # driver.quit()        
    
    sys.exit(1)

print('Will cast elem796875 to Select')
sel = Select(elem796875)
sel.select_by_value('40')

print('Selected 12/5/2023 9:40')

# id="SELECTOR_6" value=pm
failed = False
try:
    elem7984375 = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.ID, 'SELECTOR_6'))
    )
    print(f'elem7984375\n{elem7984375}')
    print(f'dir(elem7984375)\n{dir(elem7984375)}')
    print(f'elem7984375.accessible_name\n\'{elem7984375.accessible_name}\'')
    print(f'elem7984375.id\n\'{elem7984375.id}\'')
    print(f'elem7984375.tag_name\n\'{elem7984375.tag_name}\'')
    
except Exception as exc:
    print(type(exc).__name__ + ': ' + str(exc))
    failed = True
    
finally:
    if failed:
        print('FAILED getting SELECTOR_6')        
    else:
        print('DONE getting SELECTOR_6')
    # driver.quit()

if failed: 
    # driver.quit()        
    
    sys.exit(1)

print('Will cast elem7984375 to Select')
sel = Select(elem7984375)
sel.select_by_value('pm')

print('Selected 12/5/2023 9:40 pm')

# data-testid="scheduledConfirmationPrimaryAction"
# class="css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0"
failed = False
try:
    elem8 = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, '[data-testid=scheduledConfirmationPrimaryAction]'))
    )
    print(f'elem8\n{elem8}')
    print(f'dir(elem8)\n{dir(elem8)}')
    print(f'elem8.accessible_name\n\'{elem8.accessible_name}\'')
    print(f'elem8.id\n\'{elem8.id}\'')
    print(f'elem8.tag_name\n\'{elem8.tag_name}\'')
    
except Exception as exc:
    print(type(exc).__name__ + ': ' + str(exc))
    failed = True
    
finally:
    if failed:
        print('FAILED getting Schedule button')        
    else:
        print('DONE getting Schedule button')
    # driver.quit()

if failed: 
    # driver.quit()        
    
    sys.exit(1)

actions.move_to_element(elem8)
actions.click(elem8)
actions.perform()

# data-testid="tweetButton"
failed = False
try:
    elem6 = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, '[data-testid="tweetButton"]'))
    )
    print(f'elem6\n{elem6}')
    print(f'dir(elem6)\n{dir(elem6)}')
    print(f'elem6.accessible_name\n\'{elem6.accessible_name}\'')
    print(f'elem6.id\n\'{elem6.id}\'')
    print(f'elem6.tag_name\n\'{elem6.tag_name}\'')
    
except Exception as exc:
    print(type(exc).__name__ + ': ' + str(exc))
    failed = True
    
finally:
    if failed:
        print('FAILED getting Tweet button')        
    else:
        print('DONE getting Tweet button')
    # driver.quit()

if failed: 
    # driver.quit()        
    
    sys.exit(1)

print('Will submit the tweet.')
actions.move_to_element(elem6)
actions.click(elem6)
actions.perform()

# driver.close()
