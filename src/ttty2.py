#! /usr/bin/env python3

## 
 # tiny resources of the tty interface available in Python
 # 
 # From<br/>
 # <ul>
 #  <li>
 #      <a href="https://stackoverflow.com/questions/20894969/python-reading-and-writing-to-tty">
 #          linux - Python reading and writing to tty
 #      </a>
 #  </li>
 # </ul>
 # 
 #

import os # open(), read(), write()

tty = os.open ('/dev/tty', os.O_RDWR)
print ("os.open() returned: {0}".format (tty))

rv = os.write (tty, b"++ver\n")
print ("os.write() returned: {0}".format (rv))

rv = os.read (tty, 100)
print ("os.read() returned: {0}".format (rv))
