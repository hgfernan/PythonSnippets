#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 29 18:51:09 2021

From https://www.py4u.net/discuss/251206

Slightly adapted by hilton
"""

import sys # argv, exit()

from PyQt5.QtWidgets import QMessageBox as MBox, QApplication
from PyQt5.QtCore import QTimer


class TimedMBox(MBox):
    """
    Variant of QMessageBox that automatically clicks the default button
    after the timeout is elapsed
    """
        
    def __init__(self, timeout=5, buttons=None, **kwargs):
        fromStrToButton = {'Retry': MBox.Retry, 'Abort': MBox.Abort, 
                           'Cancel': MBox.Cancel, 'Ok': MBox.Ok}

        if buttons is None:
            buttons = [MBox.Retry, MBox.Abort, MBox.Cancel]

        self.timer = QTimer()
        self.timeout = timeout
        self.timer.timeout.connect(self.tick)
        self.timer.setInterval(1000)
        super(TimedMBox, self).__init__(parent=None)

        if "text" in kwargs:
            self.setText(kwargs["text"])
        if "title" in kwargs:
            self.setWindowTitle(kwargs["title"])
        
        defBt = buttons.pop(0)
        if type(defBt) == type(''):
            defBt = fromStrToButton[defBt]
        
        self.defBt = self.addButton(defBt)
        self.defBt_text = self.defBt.text()        
        self.setDefaultButton(self.defBt)
        
        for button in buttons:
            bt = button
            if type(bt) == type(''):
                bt = fromStrToButton[bt]
                
            self.addButton(bt)

    def showEvent(self, e):
        super(TimedMBox, self).showEvent(e)
        self.tick()
        self.timer.start()

    def tick(self):
        self.timeout -= 1
        if self.timeout >= 0:
            self.defBt.setText(self.defBt_text + " (%i)" % self.timeout)
        else:
            self.timer.stop()
            self.defaultButton().animateClick()

    @staticmethod
    def question(**kwargs):
        """
        Ask user a question, which has a default answer. The default answer is
        automatically selected after a timeout.

        Parameters
        ----------

        title : string
            Title of the question window

        text : string
            Textual message to the user

        timeout : float
            Number of seconds before the default button is pressed

        buttons : {MBox.DefaultButton, array}
            Array of buttons for the dialog, default button first

        Returns
        -------
        button : MBox.DefaultButton
            The button that has been pressed
        """
        w = TimedMBox(**kwargs)
        w.setIcon(MBox.Question)
        return w.exec_()

def main(argv):
    app = QApplication(argv)
    w = TimedMBox.question(text="Please select something",
                           title="Timed Message Box",
                           timeout=8)
    if w == MBox.Retry:
        print("Retry")
    if w == MBox.Cancel:
        print("Cancel")
    if w == MBox.Abort:
        print("Abort")
        
    mb = TimedMBox(timeout=3, buttons=[MBox.Ok], title="Timed Message Box", 
                   text="Just to wait a little time")
    
    mb.exec_()
    
    # Normal function termination
    return app.exit(0)
#    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))