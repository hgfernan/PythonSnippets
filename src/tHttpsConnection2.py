'''
Created on Oct 27, 2017

@author: hilton
From: 

Status: FAILED for cex.io -- Status 301

BUG: PyDev insists that HTTPSConnection is an undefined variable, 
     but Python3 uses it normally 
'''

import http.client

# "https://cex.io/api/ticker/BTC/USD"
conn = http.client.HTTPSConnection ("cex.io")
conn.request ("GET", "/api/ticker/BTC/USD")

r1 = conn.getresponse()
print(r1.status, r1.reason)
# 200 OK

data1 = r1.read() 
print (data1)