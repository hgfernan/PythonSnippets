#! /usr/bin/env python3

"""
    from "14.1. csv — CSV File Reading and Writing"
        https://docs.python.org/3.5/library/csv.html
"""

import sys # argv, exit() 
import csv # classes DictReader, DictWriter

def main(argv):
    with open('names.csv', 'w') as csvfile:
        fieldnames = ['first_name', 'last_name']
        writer = csv.DictWriter(csvfile, fieldnames = fieldnames)
        
        print("writer.fieldnames: {0}".format(writer.fieldnames))
    
        writer.writeheader()
        writer.writerow({'first_name': 'Baked', 'last_name': 'Beans'})
        writer.writerow({'first_name': 'Lovely', 'last_name': 'Spam'})
        writer.writerow({'first_name': 'Wonderful', 'last_name': 'Spam'})
        
#        print( "dir(writer): {0}".format(dir(writer)) )
    
    csvfile.close()
    
    with open('names.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        
        print("reader.fieldnames: {0}".format(reader.fieldnames))
        
        print("Before reading: line_num {}".format(reader.line_num))
        while True:
            try:
                row = next(reader)
                print(reader.line_num, row)
            except StopIteration:
                print("Finished")
                break
            
            
    csvfile.close()
        
    with open('names.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        
        for row in reader:
            print(row['first_name'], row['last_name'])
            print(reader.line_num)

#        print( "dir(reader): {0}".format(dir(reader)) )
        print("reader.fieldnames: {0}".format(reader.fieldnames))
        
        try:
            row = next(reader)
        except StopIteration as si:
            print("Could not go after last row\n'{}'".format(si))
            print("row == '{}'".format(row))
            print("line_num {}".format(reader.line_num))
#            print(dir(csvfile))
            print(csvfile.errors)
            print(csvfile.tell())
        
    # Normal function termination
    return 0

if __name__ == "__main__": 
    sys.exit(main(sys.argv))