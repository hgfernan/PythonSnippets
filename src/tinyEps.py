#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 07:05:36 2024

@author: hilton
"""

eps = 1.0

while (eps + 1.0) != 1.0: 
    eps = eps / 2 
    
print(eps)