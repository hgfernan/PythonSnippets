'''
Created on Oct 25, 2017

@author: hilton
'''

import singleton

class SingletonDeriv (metaclass=singleton.Singleton):
    def __init__ (self):
        print ("{0}.__init__ () was called".format (self.__class__.__name__))
        self.a = 10 

    def setA(self, a):
        self.a = a 
	
    def aFunc (self):
        print (f"self.aFunc() was called: a {self.a}")

if __name__ == '__main__':
    sd1 = SingletonDeriv ()
        
    print("type (sd1) == {0}" .format(type (sd1)))
    print(f"sd1 {sd1}")
    sd1.aFunc ()
    sd1.setA(15)
    sd1.aFunc ()
    
    sd2 = SingletonDeriv ()
    print ( "type (sd2) == {0}" .format(type (sd2)) )
    print(f"sd2 {sd2}")
    sd2.aFunc ()
    sd1.aFunc ()
