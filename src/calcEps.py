#! /usr/bin/env python3

import sys # argv, exit()

def calcEpsilonAndIter ():
	eps   = 0.5
	niter = 0
	while ((eps + 1.0) != 1.0):
		eps   *= 0.5		
		niter += 1
#		print (niter, eps)

	return (eps, niter)
	
def testSumEps (numb, eps):
	rv = numb + eps 

	if (rv != numb):
#		print ("numb {1:18.11e}, rv {0:18.11e}\n".format (numb, rv))
		return 0

	return 1

def main (argv):
	eps, niter = calcEpsilonAndIter () 

	print ("at {0:3d}, eps == {1:18.11e}\n".format (niter, eps))
	
	limit  = 100 * 1000
	passed = 0

	for n in range (limit):
		numb    = float (n + 1)   
		verdict = "failed"  
		if (testSumEps (numb, eps)):
			passed  += 1	
			verdict  = "passed" 	

		remain = (n + 1) % 1000
		if (remain == 0):
			print ("Eps test {0}  for {1:3.0f}".format (verdict, numb))	
	
	print ("\nPassed in {0} tests of {1}\n".format (passed, limit))
 
	return 0

if __name__ == '__main__':
	sys.exit (main (sys.argv))

