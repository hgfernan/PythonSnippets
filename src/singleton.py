'''
Created on Oct 25, 2017

@author: hilton
'''

class Singleton (type):
    _instances = {}
    def __call__(self, *args, **kwargs):
        cls = self.__class__
        if cls not in cls._instances:
            cls._instances[cls] = \
                super (Singleton, cls).__call__ (self, *args, **kwargs)
        else:
            cls._instances[cls].__init__(*args, **kwargs)
                    
        return cls._instances[cls]

# class Logger (metaclass = Singleton):
#     pass