#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Classical balance line using lists

Created on Sun Jul 16 18:02:42 2023

@author: hilton
"""

import sys # argv, exit()

from typing import List

def main(argv : List[str]) -> int:
    list1 : list[int] = [10, 15, 20, 30]
    list2 : list[int] = [10, 15, 20]
    
    ind1 = 0 
    ind2 = 0
    
    while (ind1 < len(list1)) and (ind2 < len(list2)):
        if list1[ind1] < list2[ind2]:
            print(f'list1[{ind1}] {list1[ind1]}')
            ind1 += 1
            
            continue
        
        if list1[ind1] > list2[ind2]:
            print(f'list2[{ind2}] {list2[ind2]}')
            ind2 += 1
            
            continue
        
        # OBS both are equal
        print(f'list1[{ind1}] {list1[ind1]}')
        ind1 += 1
        
        print(f'list2[{ind2}] {list2[ind2]}')
        ind2 += 1
        

    while ind1 < len(list1): 
        print(f'list1[{ind1}] {list1[ind1]}')
        ind1 += 1
        
    while ind2 < len(list2): 
        print(f'list2[{ind2}] {list2[ind2]}')
        ind2 += 1
        
    
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
