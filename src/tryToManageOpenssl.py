#! /usr/bin/env python3 

## 
 # A script from Python documentation that starts a shell, and saves everything typed in 
 # it in a file.
 # 
 # Slightly changed 
 # From<br/>
 # <ul>
 #      <li>
 #          <a href="https://docs.python.org/3.5/library/pty.html">
 #              35.8. pty — Pseudo-terminal utilities
 #          </a>
 #      </li>
 # </ul>
 # 

import os
import pty
import sys  # exit()
import time # asctime ()

executable = './startPretend2BeOpenssl.sh'
    
filename = 'dump.txt'
mode = 'wb'

with open (filename, mode) as script:
    def read(fd):
        # 1st line 
        data = os.read (fd, 1024) 
        script.write (data)
        script.flush ()
        
        # 1st answer 
        os.write (fd, b'\n')
        script.write (b'\n')
        
        # 2nd line 
        data = os.read (fd, 1024) 
        script.write (data)
        script.flush ()
        
        # 2nd answer 
        os.write (fd, b'\n')
        script.write (b'\n')
        
        return data

    print ('Script started, file is', filename)
    script.write (('Script started on %s\n' % time.asctime ()).encode ())

    pty.spawn (executable, read)

    script.write (('Script done on %s\n' % time.asctime()).encode())
    print ('Script done, file is', filename)

script.close ()

sys.exit (0)