#! /usr/bin/env python3

# -*- coding: utf-8 -*-
"""
Created on Sun Aug 15 15:20:05 2021

@author: hilton
"""

import sys 
import pydot

# TODO graph class should be inherited 
def real_node_count(graph):
    node_names = set()
    nodes = graph.get_node_list()  
    
    for node in nodes: 
        node_name = node.get_name()
        if not (node_name in node_names):
            node_names.add(node_name)

    # Normal function termination
    return len(node_names)

def main(argv):
    graphs = pydot.graph_from_dot_file("../tables/Abrangencias.dot")
    graph = graphs[0]
    
    nodes = graph.get_node_list()  
    edges = graph.get_edge_list()
    
    print(graph)
    
#    print(dir(graph))
    name = graph.get_name()     
    n_nodes = real_node_count(graph)
    n_edges = len(edges)
    
    print(f'{name} {n_nodes} {n_edges}')

    node_freq = {}
    for node in nodes: 
        node_name = node.get_name()
        if not (node_name in node_freq):
            node_freq[node_name] = [node, 1]
    
        else:
            node_freq[node_name][1] += 1

    for key in node_freq: 
        print(f'"{key}" {node_freq[key][1]}')

    print(len(node_freq))
    
    for edge in edges: 
        print(f'{edge.get_source()} -> {edge.get_destination()}')
        
    # Normal function termination
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
