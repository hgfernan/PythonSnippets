#! /usr/bin/env python3 

import sys  # argv, exit()
import math # fabs()


def main (argv):
    result = 0

    x0    = 100.0
    xCurr = x0
    xPrev = xCurr 
    nIter = 0
    eps   = 1e-7
    err   = 1.0
    maxIter = 100

    while (nIter < maxIter) and (err > eps):
        xPrev = xCurr 
        xCurr = xPrev - ((xPrev*xPrev - x0)  / (2.0 * xPrev))

        err = math.fabs ((x0 - xCurr*xCurr) / x0)
        nIter += 1

    print ('Original number {0:.8f}, estimated square root {1:.8f}'.format (x0, xCurr))
    print ('Number of iterations: {0}'.format (nIter))

    # Normal function termination
    return result 

if __name__ == '__main__':
    sys.exit (main (sys.argv))

