#! /usr/bin/env python3

## 
 # tiny resources of the tty interface available in Python
 # 
 # From<br/>
 # <ul>
 #  <li>
 #      <a href="https://stackoverflow.com/questions/20894969/python-reading-and-writing-to-tty">
 #          linux - Python reading and writing to tty
 #      </a>
 #  </li>
 # </ul>
 # 
 #

import os  # open(), read(), write()
import sys # exit()

line = '1st line: "' + sys.argv[1] + '"\n'
bline = line.encode ('UTF-8')

tty = os.open ('/dev/tty', os.O_RDWR)
print ("os.open() returned: {0}".format (tty))

rv = os.write (tty, bline)
print ("os.write() returned: {0}".format (rv))

rv = os.read (tty, 100)
print ("os.read() returned: '{0}'".format (rv))

rv = os.write (tty, b"2nd line\n")
print ("os.write() returned: {0}".format (rv))

rv = os.read (tty, 100)
print ("os.read() returned: '{0}'".format (rv))

os.close (tty)

sys.exit (0)
