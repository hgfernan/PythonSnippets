#! /usr/bin/env python3

import sys # exit()
import pyclip # copy(), paste()

# pyclip.set_clipboard('klipper')

test_str = 'The text to be copied to the clipboard.'
print( 'Copied to the clipboard "{}"'.format(test_str) )

print("a")

print("before copy")
pyclip.copy(test_str)
print("after copy")
print("a")

resp = pyclip.paste().decode()
print("a")
# Should be 'The text to be copied to the clipboard.'
print('Pasted from the clipboard "{}"  '.format(resp))

if test_str != resp:
    fmt = 'test failed: text copied was "{}", but "{}" was pasted'
    print(fmt.format(test_str, resp))

    sys.exit(1)

print("Test was successful")
sys.exit(0)    
